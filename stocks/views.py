from django.db.models import Sum
from django.views.generic.list import ListView
from django.shortcuts import render

from .models import Stock
from moves.models import Move
from things.models import Thing
from warehouses.models import Warehouse


class StockListView(ListView):
    model = Stock


class GlobalStockListView(ListView):
    template_name = "stocks/stock_global.html"

    def get_queryset(self):
        queryset = Stock.objects.values(
            "thing__code",
            "thing__description",
            "thing__default_measure__abbreviation"
        ).annotate(Sum("quantity"))
        return queryset


class StockWarehouseListView(ListView):
    model = Stock
    template_name = "stocks/stock_warehouse.html"

    def get_queryset(self):
        warehouse = self.kwargs.get("warehouse_id")
        queryset = Stock.objects.filter(warehouse=warehouse)
        return queryset


def stock_warehouse(request, warehouse_id):
    warehouse = Warehouse.objects.get(pk=warehouse_id)
    stock = Stock.objects.filter(warehouse=warehouse)
    return render(request, 'stocks/stock_warehouse.html', {
        'object_list': stock,
        'warehouse': warehouse
    })
