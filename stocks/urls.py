from django.urls import path

from . import views


app_name = "stocks"

urlpatterns = [
    path("", views.StockListView.as_view(), name="list"),
    path("global", views.GlobalStockListView.as_view(), name="global")
]
