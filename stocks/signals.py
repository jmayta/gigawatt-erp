from django.db.models.signals import pre_save
from django.dispatch import receiver
from stocks.models import Stock
from moves.models import Move, MoveThing


# Cada vez que se cree un registro en MoveThing
# Se debe aumentar/descontar el stock del almacén destino/origen
@receiver(pre_save, sender=MoveThing)
def update_stock(sender, instance, **kwargs):
    old_demand = 0
    new_quantity = 0
    # si la instancia tiene id
    if instance.id:  # es una modificación
        # obtener la instancia anterior (antes del guardado actual)
        old_instance = MoveThing.objects.get(pk=instance.pk)
        # obtener la demanda anterior
        old_demand = old_instance.demand
    # Si es una salida
    if instance.move.type == Move.OUTPUT:
        # Intentar descontar el stock del producto en el almacén origen
        try:
            # obtener stock de artículo en almacén origen
            stock = Stock.objects.get(
                thing=instance.thing,
                warehouse=instance.move.origin
            )
            # nuevo stock es igual a la diferencia del stock disponible y la
            # demanda
            stock.quantity = stock.quantity - (instance.demand - old_demand)
            # si el nuevo stock es negativo
            if stock.quantity < 0:
                # lanzar excepción por stock insuficiente
                raise Exception("El stock es insuficiente.")
                # de lo contrario
            # guardar stock
            stock.save()
        except Stock.DoesNotExist:
            raise Exception(
                "El artículo no existe en el stock actual.") from None
    # Si es una entrada
    if instance.move.type == Move.INPUT:
        # Obtener o crear existencia de artículo en almacén
        stock, created = Stock.objects.get_or_create(
            thing=instance.thing,
            warehouse=instance.move.destination,
            defaults={"quantity": instance.demand}
        )
        if not created:
            stock.quantity = stock.quantity + (instance.demand - old_demand)
            stock.save()
