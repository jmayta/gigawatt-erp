from decimal import Decimal

from django.utils.translation import gettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import ArrayField
from django.db import models

from things.models import Thing
from universal.models import UnitOfMeasurement
from warehouses.models import Warehouse

from sunqu.behaviors import Timestampable


class Stock(Timestampable, models.Model):
    # Artículo
    thing = models.ForeignKey(
        Thing,
        on_delete=models.PROTECT,
        verbose_name=_("thing")
    )
    # Almacén
    warehouse = models.ForeignKey(
        Warehouse,
        on_delete=models.CASCADE,
        verbose_name=_("warehouse")
    )
    # La cantidad funciona con la <unidad de medida> predeterminada asignada al
    # artículo.
    quantity = models.FloatField(_("quantity"), default=0.00)

    class Meta:
        verbose_name = _("stock")
        verbose_name_plural = _("stocks")

    def __str__(self):
        return f"{self.warehouse} / {self.thing}"
