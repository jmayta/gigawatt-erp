from decimal import Decimal

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model

from contacts.models import Contact
from partners.models import Partner
from sunqu.behaviors import Timestampable
from things.models import Thing
from universal.models import Currency, UnitOfMeasurement

User = get_user_model()


class Quotation(Timestampable, models.Model):
    # Número de referencia
    numeration = models.CharField(
        _("numeration"),
        blank=True,
        default="",
        max_length=50
    )
    # Proveedor
    vendor = models.ForeignKey(
        Partner,
        on_delete=models.CASCADE,
        related_name="quotations",
        verbose_name=_("vendor")
    )
    # Contacto (opcional)
    contact = models.ForeignKey(
        Contact,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("contact")
    )
    # Moneda
    currency = models.ForeignKey(
        Currency,
        on_delete=models.PROTECT,
        verbose_name=_("currency")
    )
    # Monto sin impuestos incluidos
    amount_untaxed = models.DecimalField(
        _("untaxed amount"),
        max_digits=14,
        decimal_places=3,
        default=Decimal(0.000)
    )
    # Monto de impuesto
    amount_tax = models.DecimalField(
        _("tax amount"),
        max_digits=14,
        decimal_places=3,
        default=Decimal(0.000)
    )
    # Monto total
    amount_total = models.DecimalField(
        _("total amount"),
        max_digits=14,
        decimal_places=3,
        default=Decimal(0.000)
    )
    # Términos y condiciones
    terms_conditions = models.TextField(
        _("terms & conditions"),
        blank=True,
        default=""
    )
    # Colaborador (quien registró la cotización)
    collaborator = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="quotations",
        verbose_name=_("collaborator")
    )

    class Meta:
        verbose_name = _("quotation")
        verbose_name_plural = _("quotations")

    def __str__(self):
        return f"{self.id}"


class QuotationThing(models.Model):
    # Cotización
    quotation = models.ForeignKey(
        Quotation,
        on_delete=models.CASCADE,
        related_name="things"
    )
    # Código externo
    external_code = models.CharField(
        _("external code"),
        blank=True,
        default="",
        max_length=50
    )
    # Cantidad
    quantity = models.FloatField(
        _("quantity"),
        default=0.00
    )
    # Unidad de Medida
    uom = models.ForeignKey(
        UnitOfMeasurement,
        on_delete=models.PROTECT,
        verbose_name=_("unit of measurement")
    )
    # Artículo
    thing = models.ForeignKey(
        Thing,
        on_delete=models.PROTECT,
        related_name="quoted",
        verbose_name=_("thing")
    )
    # Descripción
    description = models.CharField(
        _("description"),
        blank=True,
        default="",
        max_length=255
    )
    # Precio unitario
    unit_price = models.DecimalField(
        _("unit price"),
        decimal_places=3,
        default=Decimal(0.000),
        max_digits=14
    )
    # Valor unitario
    unit_value = models.DecimalField(
        _("unit value"),
        decimal_places=3,
        default=Decimal(0.000),
        max_digits=14
    )
    # Monto con impuestos incluidos (Valor de venta)
    subtotal = models.DecimalField(
        _("amount tax inclusive"),
        decimal_places=3,
        default=Decimal(0.000),
        max_digits=14
    )

    class Meta:
        verbose_name = _("quotation thing")
        verbose_name_plural = _("quotation things")

    def __str__(self):
        return f"{self.id}"
