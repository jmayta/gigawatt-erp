from django.contrib import admin

from .models import Quotation, QuotationThing


class QuotationThingInline(admin.TabularInline):
    model = QuotationThing


class QuotationAdmin(admin.ModelAdmin):
    inlines = [
        QuotationThingInline,
    ]


admin.site.register(Quotation, QuotationAdmin)
