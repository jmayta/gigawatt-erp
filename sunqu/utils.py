from datetime import datetime, timedelta
from typing import Optional, List, Tuple
import os
import re

from django.db.models import Model

__base36 = (
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
    'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
    'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
)


def generate_integer_code(klass, *args: str) -> int:
    """Genera códigos consecutivos anuales de tipo entero"""
    last_code = 0
    new_code = 1
    last_object = None

    objects_count = klass.objects.count()

    if objects_count > 0:
        last_object = klass.objects.latest(*args)
        last_code = last_object.code
        # si el <año actual> es el mismo que el año en el que fue creado el
        # último código
        if datetime.now().year == last_object.created_at.year:
            # seguir el correlativo
            new_code = last_code + 1

    return new_code


def add_business_days(date, days=0):
    while days > 0:
        date += timedelta(days=1)
        if date.weekday() >= 5:
            continue
        days -= 1
    return date


def base36_to_number(some_string):
    string_array = list(some_string)
    string_array.reverse()
    number = 0
    for i, value in enumerate(string_array):
        number = number + __base36.index(value) * 36 ** i
    return number


def codify(number, length=4):
    """ Retorna una cadena tipo código de 'length' caracteres. """
    string_code = str(number)
    missing = length - len(string_code)
    if missing > 0:
        string_array = list(string_code)
        for value in range(0, missing):
            string_array.insert(0, '0')
        return "".join(string_array)
    else:
        return -1


def get_env_variable(var_name):
    """Get the environment variable or return exception."""
    from django.core.exceptions import ImproperlyConfigured
    try:
        return os.environ[var_name]
    except KeyError:
        error_msg = f"Set the {var_name} environment variable."
        raise ImproperlyConfigured(error_msg)


def number_to_base36(number):
    """Decimal to base36."""
    container = []
    while number >= 36:
        mod = number % 36
        container.append(mod)
        number = number // 36
    else:
        container.append(number)
    container.reverse()
    for i, value in enumerate(container):
        container[i] = __base36[value]
    return "".join(container)


def get_last_entry(klass, *args):
    """Obtiene el último registro de un modelo basado a un atributo"""
    last_entry = klass.objects.latest(*args)
    return last_entry


def annual_auto_reset_code(model, *fields):
    '''
    Retorna un número entero que corresponde al código consecutivo de un
    modelo, si el último código fue registrado en un año anterior, éste retorna
    el primer número entero disponible (el número 1).
    '''
    # Inicializar variables
    last_code = 0
    new_code = 0
    this_year = datetime.utcnow().year

    # obtener cantidad de registros de este año
    total_entries = model.objects.filter(
        created_at__year=this_year
    ).count()
    # condición
    if total_entries > 0:
        # si la cantidad de registros es mayor a 0 ...
        # obtener el ultimo registro basado a campo(s) específico(s)
        last_entry = model.objects.latest(*fields)
        # obtener el código del último registro
        last_code = last_entry.code
    # crear el nuevo código consecutivo
    new_code = last_code + 1
    return new_code


class NumberLiteral:

    __units_literals = (
        None, "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve",
    )
    __tens_literals = (
        "diez", "once", "doce", "trece", "catorce", "quince", "dieciséis", "diecisiete", "dieciocho", "diecinueve",
        "veinte", "veintiuno", "veintidós", "veintitrés", "veinticuatro", "veinticinco", "veintiséis", "veintisiete",
        "veintiocho", "veintinueve",
    )
    __tens_literals_x = (
        "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa",
    )
    __hundreds_literals = (
        "ciento", "doscientos", "trescientos", "cuatrocientos", "quinientos", "seiscientos", "setecientos", "ochocientos",
        "novecientos",
    )

    __hundreds = (
        "cien", "ciento"
    )
    __thousands = (
        "mil", "miles"
    )
    __millions = (
        "millón", "millones",
    )
    __billions = (
        "billón", "billones",
    )

    __position = (
        __hundreds, __thousands, __millions, __billions,
    )

    number: str = ""

    def __init__(self, number):
        self.number = number
        valid_number = self.validate(number)
        fragments = self.fragment(valid_number)
        self.to_literal(fragments)

    @staticmethod
    def validate(number: str) -> Optional[str]:
        valid_number: str
        # captura enteros[.decimales]
        pattern = re.compile(r"\d{1,12}(?:\.\d{2})?")
        # obtener coincidencia exacta
        match = pattern.fullmatch(number)
        if match:
            valid_number = match.group(0)
            # si el número es entero (no tiene parte decimal) se le agrega ".00" al final
            if valid_number.find(".") == -1:
                valid_number += ".00"
            # retornar número validado
            return valid_number
        else:
            # si no hay coincidencia lanzar mensajes y concluir
            print("El número no cuenta con el formato requerido.")
            print("El formato debe ser un número expresado como entero o con dos dígitos decimales.")
            print("ex: 15000 ó 15000.20")
            return None

    @staticmethod
    def fragment(valid_number: str) -> List:
        # separar enteros y decimales
        int_fragment, float_fragment = valid_number.split(".")  # "18021.30" -> ["18021", "30"]
        int_fragment_reversed = int_fragment[::-1]  # "12081"
        # capturar bloques de 3 en 3
        pattern = re.compile(r"\d{1,3}")
        matches = pattern.findall(int_fragment_reversed)  # ["021", "81"]
        fragments = [fragment[::-1] for fragment in matches]  # ["021", "18"]
        return fragments

    def to_literal(self, fragments):
        for fragment in fragments:
            print("fragment: ", fragment)
            n = int(fragment)  # 210
            s = fragment.lstrip("0")  # "210"
            if n < 10:
                if n == 1:
                    print(self.__units_literals[n], self.__position[fragments.index(fragment)])
            elif n < 100:
                print(self.__tens_literals[n], self.__position[fragments.index(fragment)])
            elif n < 1000:
                print(self.__hundreds_literals[n // 100 - 1], self.__position[fragments.index(fragment)])
        return
