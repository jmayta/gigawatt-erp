# Sunqu

Navaja suiza.

## Funciones disponibles

### annual_auto_reset_code()
Retorna un número entero que corresponde al código consecutivo de un modelo, si el último código fue registrado en un año anterior, éste retorna el primer número entero disponible (el número 1).

**Ejemplo:**

El último código registrado en uno de nuestros modelos fue `5` y se realizó el 2018-12-29 (29 de diciembre del 2018).

- Caso 1: Ejecutamos la función el 2018-12-30 y se nos retornará el código `6`.
- Caso 2: Ejecutamos la función el 2019-01-01 y se nos retornará el código `1`.

En el primer caso, se ejecuta la función en el mismo año, es por eso que devuelve el valor consecutivo. En el segundo caso, se ejecuta la función el año próximo al último registro, es por ello que el código se reinicia y automáticamente devuelve el primer valor disponible, el número 1.