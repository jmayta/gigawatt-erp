from django.utils.translation import gettext_lazy as _
from django.contrib.contenttypes.fields import (
    GenericForeignKey,
    GenericRelation
)
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone

User = get_user_model()


class Applicable(models.Model):
    applicant = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        verbose_name=_("applicant")
    )

    class Meta:
        abstract = True


# Beta
class Authorizable(models.Model):
    # Autorizaciones
    authorizations = GenericRelation("Authorization")

    class Meta:
        abstract = True


class OptionallyGenericRelateable(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")

    class Meta:
        abstract = True


class Timestampable(models.Model):
    """
    An abstract base class model that provides self-updating ``created`` and
    ``modified`` fields.
    """
    created_at = models.DateTimeField(_("created at"), blank=True, null=True)
    updated_at = models.DateTimeField(_("updated at"), blank=True, null=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        # si no tiene id aun..
        if not self.id:
            # timestamp de creación
            self.created_at = timezone.now()
        # timestamp de modificación
        self.updated_at = timezone.now()
        return super().save(*args, **kwargs)
