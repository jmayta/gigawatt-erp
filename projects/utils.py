from django.db.models import QuerySet


def active_projects() -> QuerySet:
    from .models import Project
    projects = Project.objects.exclude(is_active=False)
    return projects
