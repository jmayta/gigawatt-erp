from django.urls import include, path

from . import views


app_name = "projects"


requisitions_patterns = []

urlpatterns = [
    # Proyectos
    # -------------------------------------------------------------------------
    path("", views.ProjectList.as_view(), name="list"),
    path("<int:pk>/", views.detail, name="detail"),
    path("create/", views.ProjectCreate.as_view(), name="create"),

    # Proyectos & Requerimientos
    # -------------------------------------------------------------------------
    path(
        "<int:pk>/requisitions/",
        views.ProjectRequisitionList.as_view(),
        name="requisitions"
    ),
    path(
        "<int:pk>/requisitions/master",
        views.ProjectRequisitionMasterList.as_view(),
        name="requisitions_master"
    ),

    # Proyectos & Compras
    # -------------------------------------------------------------------------
    # path(
    #     "<int:pk>/purchases/",
    #     views.ProjectPurchaseList.as_view(),
    #     name="purchases"
    # ),
]
