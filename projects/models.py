import uuid as uuid_lib

from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _

from partners.models import Partner
from sunqu.behaviors import Timestampable
from universal.models import Currency, UnitOfMeasurement
from things.models import Thing

User = get_user_model()


class Project(Timestampable, models.Model):
    """ Proyecto """
    # uuid
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    # Alias
    alias = models.CharField(max_length=100)
    # Nombre
    name = models.TextField(_("name"), blank=True, default="")
    # ¿Es propio?
    is_owned = models.BooleanField(_("is owned"), default=False)
    # Fecha de adjudicación
    award_date = models.DateField(_("award date"), blank=True, null=True)
    # Cliente
    customer = models.ForeignKey(
        Partner,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="projects",
        verbose_name=_("customer")
    )
    # Proyecto padre
    parent = models.ForeignKey(
        "self",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="children",
        verbose_name=_("parent project")
    )
    # ¿Está activo?
    is_active = models.BooleanField(
        _("is active"),
        default=True
    )
    # Miembros (equipo)
    members = models.ManyToManyField(
        User,
        through="Membership",
        verbose_name=_("members")
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["alias"],
                name="unique_project_alias"
            )
        ]
        verbose_name = _("project")
        verbose_name_plural = _("projects")

    def __str__(self):
        return f"{self.alias}"


class Membership(models.Model):
    """Membresía"""
    # Proyecto
    project = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        verbose_name=_("project"),
    )
    # Miembro
    member = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_("member"),
    )
    # Cargo / Puesto
    position = models.CharField(
        _("position"),
        blank=True,
        default="",
        max_length=254
    )
    # Es líder de equipo
    is_leader = models.BooleanField(
        _("is leader"),
        default=False
    )
    # Fecha de ingreso
    joining_date = models.DateField(_("joining date"))
    # Fecha de salida
    leaving_date = models.DateField(_("leaving date"), blank=True, null=True)

    class Meta:
        verbose_name = _("membership")
        verbose_name_plural = _("memberships")

    def __str__(self):
        return f"{self.project} // {self.member}"
