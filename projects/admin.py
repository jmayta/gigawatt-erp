from django.contrib import admin
from django.http import HttpResponseRedirect
from django.shortcuts import reverse

from import_export.admin import ImportExportModelAdmin

from .forms import ProjectForm, MemberForm
from .models import Project, Membership


class MembershipInLine(admin.TabularInline):
    model = Membership


class ProjectAdmin(ImportExportModelAdmin):
    form = ProjectForm
    inlines = (
        MembershipInLine,
    )
    search_fields = (
        "alias",
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # Si el nombre del campo es...
        if db_field.name == "customer":
            from partners.models import Partner
            # Sobrescribir el queryset
            # Mostrar sólo clientes
            kwargs["queryset"] = Partner.objects.filter(is_customer=True)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def response_post_save_add(self, request, obj):
        return HttpResponseRedirect(reverse("projects:list"))


admin.site.register(Project, ProjectAdmin)
