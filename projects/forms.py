from django import forms
from django.utils.translation import gettext_lazy as _

from .models import Project, Membership


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = (
            "alias",
            "name",
            "is_owned",
            "award_date",
            "customer",
            "parent",
            "is_active",
        )
        help_texts = {
            "name": _("Name of project"),
            "is_owned": _(
                "Mark if it's an internal project"
            ),
            "parent": _("Parent project")
        }


class MemberForm(forms.ModelForm):
    class Meta:
        model = Membership
        fields = (
            "member",
            "joining_date",
            "leaving_date",
            "position",
            "is_leader",
        )
