from django.urls import path

from projects.api import views


urlpatterns = [
    # /api/v1/projects/
    path("", views.ListCreateProject.as_view(), name="project-list"),
    # /api/v1/projects/<pk>
    path(
        "<int:pk>/",
        views.RetrieveUpdateDestroyProject.as_view(),
        name="project-list"
    ),
]
