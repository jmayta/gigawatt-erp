from django.db.models import F, Q, Sum
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView, CreateView, UpdateView

# from purchases.models import Purchase, PurchaseLine
from requisitions.models import Requisition, RequisitionThing

from .forms import ProjectForm
from .models import Project


# Requisitions
# -----------------------------------------------------------------------------

class ProjectList(ListView):
    model = Project


class ProjectCreate(CreateView):
    model = Project
    form_class = ProjectForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class ProjectDetail(DetailView):
    model = Project

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

def index(request):
    projects = Project.objects.all()
    context = {
        "projects": projects,
    }
    return render(request, "projects/project_list.html", context)


def detail(request, pk):
    project = Project.objects.get(pk=pk)
    total_requisitions = project.requisitions.count()
    total_purchases = project.purchases.count()
    context = {
        "project": project,
        "total_purchases": total_purchases,
        "total_requisitions": total_requisitions
    }
    return render(
        request, "projects/project_detail.html", context)


# def global_purchase(request, pk):
#     project = get_object_or_404(Project, pk=pk)
#     project_purchases = Purchase.objects.filter(project__id=pk)
#     project_purchases_amount = project_purchases.aggregate(
#         total=Sum("amount_untaxed")
#     )
#     context = {
#         "project": project,
#         "project_purchases": project_purchases,
#         "total_purchased": project_purchases_amount["total"]
#     }
#     return render(request, "projects/project_purchase.html", context)


# def purchase_detail(request, pk):
#     project = get_object_or_404(Project, pk=pk)
#     purchase_details = PurchaseLine.objects.filter(
#         purchase__project=pk).order_by("-thing__description")
#     purchase_details = purchase_details.annotate(
#         subtotal=F("quantity_demanded") * F("price_unit"))
#     total_purchased = purchase_details.aggregate(total=Sum("subtotal"))
#     context = {
#         "project": project,
#         "purchase_details": purchase_details,
#         "total_purchased": total_purchased
#     }
#     return render(request, "projects/project_purchase_detail.html", context)


# Requisitions
# -----------------------------------------------------------------------------
class ProjectRequisitionList(ListView):
    template_name = "projects/project_requisition_list.html"

    def get_queryset(self):
        self.project = get_object_or_404(Project, pk=self.kwargs.get("pk"))
        queryset = self.project.requisitions.all()
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["project"] = self.project
        return context


class ProjectRequisitionMasterList(ListView):
    template_name = "projects/project_requisition_master_list.html"

    def get_queryset(self):
        self.project = get_object_or_404(Project, pk=self.kwargs.get("pk"))
        project_requisition_lines = RequisitionThing.objects.filter(
            requisition__project=self.project
        )
        requisition_lines = project_requisition_lines.values(
            "thing",
            "thing__description",
            "thing__default_measure__abbreviation",
        ).annotate(
            qty_pending=Sum(F("demand")-F("quantity_attended"))
        ).filter(qty_pending__gt=0).order_by("thing__description")
        self.queryset = requisition_lines
        return self.queryset


# Purchases
# -----------------------------------------------------------------------------
# class ProjectPurchaseList(ListView):
#     template_name = "projects/project_purchase_list.html"

#     def get_queryset(self):
#         self.project = get_object_or_404(Project, pk=self.kwargs.get("pk"))
#         self.queryset = Purchase.objects.filter(project=self.project)
#         return super().get_queryset()

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context["object"] = self.project
#         return context
