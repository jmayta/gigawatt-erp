from django.contrib.postgres import fields
from django.db import models
from django.utils.translation import gettext_lazy as _

from sunqu.behaviors import Timestampable


class Contact(Timestampable, models.Model):
    # Nombre
    name = models.CharField(_("name"), max_length=255)
    # Correo electrónico
    email = models.EmailField(blank=True, default="")
    # Telefonos
    phones = fields.ArrayField(
        models.CharField(blank=True, max_length=25),
        blank=True,
        null=True
    )
    # Sucursal
    branch_office = models.CharField(blank=True, default="", max_length=255)
    # Título/Cargo
    title = models.CharField(blank=True, default="", max_length=255)
    # Comentario/observación
    comment = models.TextField(blank=True, default="")
    # Fecha de baja
    deactivation_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        verbose_name = _("contact")
        verbose_name_plural = _("contacts")

    def __str__(self):
        return f"{self.name}"
