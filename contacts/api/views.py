from rest_framework import generics

from .serializers import ContactSerializer

from ..models import Contact
from partners.models import Partner


class ContactList(generics.ListAPIView):
    serializer_class = ContactSerializer

    def get_queryset(self):
        try:
            partner_pk = self.request.query_params.get("partner", None)
            partner = Partner.objects.get(pk=partner_pk)
        except Partner.DoesNotExist:
            pass
        else:
            queryset = partner.contacts.all()
            return queryset
        return []


class ProviderContactList(generics.ListAPIView):
    serializer_class = ContactSerializer
    model = Contact

    def get_queryset(self):
        try:
            partner_pk = self.request.query_params.get("partner", None)
            partner = Partner.objects.get(pk=partner_pk)
        except Partner.DoesNotExist:
            pass
        else:
            queryset = super().get_queryset().filter(partner=partner)
            return queryset
        return []
