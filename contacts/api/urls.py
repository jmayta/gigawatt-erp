from django.urls import path

from . import views


urlpatterns = [
    path("", views.ContactList.as_view(), name="contact-list"),
]
