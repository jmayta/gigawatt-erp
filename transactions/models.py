from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from sunqu.behaviors import Timestampable
from universal.models import Currency


class Transaction(Timestampable):
    category = models.ForeignKey(
        Category,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )
    note = models.CharField(max_length=254, default="")
    date = models.DateField(default=timezone.now())
    wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE)


class Category(models.Model):
    INCOME = "in"  # Ingreso (+)
    EXPENSE = "ex"  # Gasto (-)
    DEBT = "de"  # Deuda (+)
    LOAN = "lo"  # Préstamo (-)
    KIND_CHOICES = (
        (INCOME, _("Income")),
        (EXPENSE, _("Expense")),
        (DEBT, _("Debt")),
        (LOAN, _("Loan")),
    )
    parent = models.ForeignKey("self", on_delete=models.SET_NULL)
    name = models.CharField(_("name"), max_length=150)
    icon = models.CharField(_("icon"), max_length=254)
    kind = models.CharField(
        _("kind"),
        choices=KIND_CHOICES,
        default=INCOME,
        max_length=2,
    )

    class Meta:
        verbose_name = _("category")
        verbose_name_plural = _("categories")


class Wallet(Timestampable):
    name = models.CharField(max_length=254,)
    currency = models.ForeignKey(Currency, on_delete=models.PROTECT)

    class Meta:
        verbose_name = _("wallet")
        verbose_name_plural = _("wallets")
