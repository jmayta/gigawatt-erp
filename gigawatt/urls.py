from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path

# from core.views import IndexTemplateView

api_v1_patterns = [
    path(
        "partners/",
        include("partners.api.urls"),
        name="partners"
    ),
    path(
        "contacts/",
        include("contacts.api.urls"),
        name="contacts"
    ),
    path(
        "projects/",
        include("projects.api.urls"),
        name="projects"
    ),
    path(
        "things/",
        include("things.api.urls"),
        name="things"
    ),
    path(
        "universal/",
        include("universal.api.urls"),
        name="countries"
    ),
]

urlpatterns = [
    # Django urls
    path("accounts/", include("django.contrib.auth.urls")),
    path("admin/", admin.site.urls),

    # Third Party urls
    path("summernote/", include("django_summernote.urls")),

    # App urls
    path("moves/", include("moves.urls")),
    path("orders/", include("orders.urls")),
    path("partners/", include("partners.urls")),
    path("projects/", include("projects.urls")),
    path("requisitions/", include("requisitions.urls")),
    path("stocks/", include("stocks.urls")),
    path("things/", include("things.urls")),
    path("universal/", include("universal.urls")),
    path("warehouses/", include("warehouses.urls")),
    path("", include("website.urls")),

    # API urls
    # v1
    path(
        "api/v1/",
        include(
            (api_v1_patterns, "v1"),
            namespace="v1"
        ),
    ),
    path("api/v1/requisitions/", include("requisitions.api.urls")),

    # entry point
    # re_path(r"^.*$", IndexTemplateView.as_view(), name="entry-point"),
]

if settings.DEBUG:
    import debug_toolbar  # django-debug-toolbar

    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    # django-debug-toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
