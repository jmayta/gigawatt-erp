import os

from sunqu.utils import get_env_variable


BASE_DIR = os.path.dirname(
    os.path.dirname(
        os.path.dirname(
            os.path.abspath(__file__)
        )
    )
)
SECRET_KEY = get_env_variable("SECRET_KEY")

ALLOWED_HOSTS = ["*"]

INSTALLED_APPS = [
    # django apps -------------------------------------------------------------
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",

    # third party -------------------------------------------------------------
    "crispy_forms",
    "django_summernote",
    "import_export",
    "rest_framework",
    "rest_framework.authtoken",
    "rest_auth",
    "storages",
    "widget_tweaks",

    # project apps ------------------------------------------------------------
    "authorizations.apps.AuthorizationsConfig",
    # "budgets.apps.BudgetsConfig",
    # "cash.apps.CashConfig",
    "cashflow.apps.CashflowConfig",
    "contacts.apps.ContactsConfig",
    # "collaborators.apps.CollaboratorsConfig",
    # "invoices.apps.InvoicesConfig",
    "maintenances.apps.MaintenancesConfig",
    "expenses.apps.ExpensesConfig",
    "moves.apps.MovesConfig",
    "orders.apps.OrdersConfig",
    "partners.apps.PartnersConfig",
    "profiles.apps.ProfilesConfig",
    "projects.apps.ProjectsConfig",
    # "purchases.apps.PurchasesConfig",
    "quotations.apps.QuotationsConfig",
    "requisitions.apps.RequisitionsConfig",
    "stocks.apps.StocksConfig",
    "sunat.apps.SunatConfig",
    # "tasks.apps.TasksConfig",
    "things.apps.ThingsConfig",
    # "tracking.apps.TrackingConfig",
    "universal.apps.UniversalConfig",
    "warehouses.apps.WarehousesConfig",
    "website.apps.WebsiteConfig",
]
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]
ROOT_URLCONF = "gigawatt.urls"
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            os.path.join(BASE_DIR, "templates")
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]
WSGI_APPLICATION = "gigawatt.wsgi.application"
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": get_env_variable("DATABASE_NAME"),
        "USER": get_env_variable("DATABASE_USER"),
        "PASSWORD": get_env_variable("DATABASE_PASSWORD"),
        "HOST": get_env_variable("DATABASE_HOST"),
        "PORT": get_env_variable("DATABASE_PORT"),
    }
}
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "".join([
            "django.contrib.auth.password_validation",
            ".UserAttributeSimilarityValidator"
        ]),
    },
    {
        "NAME": "".join([
            "django.contrib.auth.password_validation",
            ".MinimumLengthValidator"
        ]),
    },
    {
        "NAME": "".join([
            "django.contrib.auth.password_validation.",
            "CommonPasswordValidator"
        ]),
    },
    {
        "NAME": "".join([
            "django.contrib.auth.password_validation",
            ".NumericPasswordValidator"
        ]),
    },
]


# language and location
LANGUAGE_CODE = "es-MX"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True


# authentication
LOGIN_URL = "/accounts/login/"
LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"


# decimal formating
DECIMAL_SEPARATOR = "."
USE_THOUSAND_SEPARATOR = True
THOUSAND_SEPARATOR = ","
NUMBER_GROUPING = 3


# django-crispy-forms
CRISPY_TEMPLATE_PACK = "bootstrap4"


# django-import-export
IMPORT_EXPORT_USE_TRANSACTIONS = True


# django.contrib.sites
SITE_ID = 1


# django-rest-framework
REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.TokenAuthentication",
    ),
    "DEFAULT_PERMISSION_CLASSES": (
        "rest_framework.permissions.IsAuthenticated",
    )
}

# Email management
DEFAULT_FROM_EMAIL = get_env_variable("DEFAULT_FROM_EMAIL")

EMAIL_HOST = get_env_variable("EMAIL_HOST")
EMAIL_HOST_PASSWORD = get_env_variable("EMAIL_HOST_PASSWORD")
EMAIL_HOST_USER = get_env_variable("EMAIL_HOST_USER")
EMAIL_PORT = get_env_variable("EMAIL_PORT")
EMAIL_USE_SSL = get_env_variable("EMAIL_USE_SSL")