from .base import *

DEBUG = False

INSTALLED_APPS += []

# AWS S3
AWS_ACCESS_KEY_ID = get_env_variable("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = get_env_variable("AWS_SECRET_ACCESS_KEY")
AWS_STORAGE_BUCKET_NAME = get_env_variable("AWS_STORAGE_BUCKET_NAME")

AWS_S3_FILE_OVERWRITE = True
AWS_DEFAULT_ACL = None

# Static Files
STATIC_URL = "/static/"

# Media Files
MEDIA_URL = "/media/"

try:
    from .local import *
except ImportError:
    MIDDLEWARE += [
        "whitenoise.middleware.WhiteNoiseMiddleware",  # whitenoise
    ]

    STATIC_ROOT = os.path.join(BASE_DIR, "assets")
    STATICFILES_DIRS = (
        os.path.join(BASE_DIR, "static"),
    )
    # -> to allow "collectstatic" put automatically static in bucket
    # STATICFILES_STORAGE = "gigawatt.storages.StaticStorage"
    # -> to upload media files to S3
    DEFAULT_FILE_STORAGE = "gigawatt.storages.MediaStorage"
    pass
