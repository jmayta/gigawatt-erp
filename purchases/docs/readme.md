## Breakpoints

### Path de Compras

Lista de puntos de acceso netamente del módulo de compras.

#### Lista de compras
> **GET** /purchases/ 

#### Crear compra
> **POST** /purchases/

#### Detalle de compra
> **GET** /purchases/1/

#### Modificar compra
> **PUT/PATCH** /purchases/

#### Eliminar compra
> **DELETE** /purchases/1/


### Intersección: User (autor)

Puntos de acceso desde `user` autor.

#### Lista de compras de `user`
> **GET** /collaborators/1/purchases/
