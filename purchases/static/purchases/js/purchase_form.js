document.addEventListener("DOMContentLoaded", function (event) {

    Vue.component('v-select', VueSelect.VueSelect);

    let app = new Vue({
        el: "#app",
        components: {
            "select2": httpVueLoader("/static/purchases/js/components/select2.vue")
        },
        data: {
            // pre-load data form
            contacts: [],
            currencies: [],
            expenseTypes: [],
            projects: [],
            providers: [],
            requisitions: [],
            things: [],
            unitsOfMeasurement: [],
            // new purchase
            newPurchase: {
                project: null,
                requisition: null,
                currency: null,
                partner_reference: "",
                partner: null,
                contact: null,
                observation: "",
                terms_conditions: "",
                purchase_lines: this.newPurchaseLines
            },
            newLine: {
                // detalle de compra
                    external_code: "",
                    demand: (0.0).toFixed(2),
                    uom: null,
                    uomDescription: "",
                    thing: null,
                    thingDescription: "",
                    price_unit: (0.0).toFixed(2),
                    igv_included: false,
                    expense_type: null,
                    subtotal: 0
            },
            newPurchaseLines: [
                // contenedor de detalles de compra
            ]
        },
        computed: {
            lineSubtotal: function () {
                let subtotal, tax = 0;
                if (this.newLine.igv_included) {
                    tax = 0.18;
                }
                subtotal = this.newLine.demand * this.newLine.price_unit * (tax + 1);
                return Number.parseFloat(subtotal).toFixed(3);
                // return 0;
            }
        },
        methods: {
            edit: function (event) {
                console.log(event);
            },
            loadContacts: function () {
                let vm = this;
                this.newPurchase.contact = null;
                if (this.newPurchase.partner) {
                    axios.get(`/api/v1/contacts/?format=json&partner=${this.newPurchase.partner}`)
                        .then(function (response) {
                            vm.contacts = response.data;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                } else {
                    this.contacts = [];
                }
            },
            loadCurrencies: function () {
                let vm = this;
                axios.get("/api/v1/universal/currencies/?format=json")
                    .then(function (response) {
                        vm.currencies = response.data;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            loadExpenseTypes: function () {
                let vm = this;
                axios.get("/api/v1/universal/xtypes/?format=json")
                    .then(function (response) {
                        vm.expenseTypes = response.data;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            loadProjects: function () {
                let vm = this;
                axios.get("/api/v1/projects/?format=json")
                    .then(function (response) {
                        vm.projects = response.data;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            loadProviders: function () {
                let vm = this;
                axios.get("/api/v1/partners/?format=json")
                    .then(function (response) {
                        vm.providers = response.data;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            loadRequisitions: function () {
                let vm = this;
                axios.get("/api/v1/requisitions/?format=json")
                    .then(function (response) {
                        vm.requisitions = response.data;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            loadThings: function () {
                let vm = this;
                axios.get("/api/v1/things/?format=json")
                    .then(function (response) {
                        vm.things = response.data;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            loadUnitsOfMeasurement: function () {
                let vm = this;
                axios.get("/api/v1/universal/uom/?format=json")
                    .then(function (response) {
                        vm.unitsOfMeasurement = response.data;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            pushNewLine: function () {
                let vm = this;
                axios.get(`/api/v1/things/${vm.newLine.thing}/?format=json`)
                    .then(function (response) {
                        vm.newLine.thingDescription = response.data.description;
                        vm.newLine.subtotal = vm.lineSubtotal;
                        vm.newPurchaseLines.push(vm.newLine);
                        vm.newLine = {};
                    });
            },
            showFormAddItem: function () {
                console.log("showFormAddItem ejecutado.")
            },
            cancel: function (event) {
                console.log("Botón \"Cancelar\" presionado.");
                window.location.href = "/purchases/";
            },
            save: function (event) {
                console.log("Botón \"Guardar\" presionado.");
            },
        },
        created: function () {
            this.loadCurrencies();
            this.loadExpenseTypes();
            this.loadProjects();
            this.loadProviders();
            this.loadRequisitions();
            this.loadThings();
            this.loadUnitsOfMeasurement();
        }
    });
});