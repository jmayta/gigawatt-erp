document.addEventListener("DOMContentLoaded", function() {
    $(".datatable-responsive").DataTable({
        responsive: {
            details: {
                type: "column"
            }
        },
        columnDefs: [
            {
                className: "control",
                defaultContent: "",
                orderable: false,
                targets: 0
            },
            {
                targets: 1,
                width: "100px"
            },
            {
                targets: 3,
                width: "380px"
            },
            {
                targets: 4,
                width: "160px"
            },
            {
                targets: 5,
                width: "130px"
            }
        ],
    });
});
