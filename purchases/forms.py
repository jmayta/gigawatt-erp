from django import forms
from django.utils.translation import gettext_lazy as _

from .models import Purchase, PurchaseLine


class PurchaseForm(forms.ModelForm):

    code = forms.IntegerField(
        label=_("Code"),
        disabled=True,
        required=False,
    )

    class Meta:
        model = Purchase
        fields = "__all__"
