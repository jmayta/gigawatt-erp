from decimal import Decimal

from django.db.models import Sum
from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver

from .models import Purchase, PurchaseLine
from .utils import generate_code
from sunat.models import Tax
from sunat.utils import current_igv_rate


@receiver(pre_save, sender=Purchase)
def assign_code(sender, instance, **kwargs):
    if instance.id:
        pass
    else:
        instance.code = generate_code()
    print(instance.code)


# Señal lanzada cada vez que se cree, actualice o elimine un registro en
# <Detalle de compra>
@receiver(pre_save, sender=PurchaseLine)
def update_purchase_line_amounts(sender, instance, **kwargs):
    # Si el check de "Igv incluido" no está marcado
    if not instance.igv_included:
        # Subtotal = Precio Unitario * Demanda (cant)
        instance.price_subtotal = instance.price_unit * instance.demand
    # De lo contrario
    else:
        # Subtotal = (Precio Unitario / (1 + IGV)) * Demanda
        instance.price_subtotal = (
            instance.price_unit / (1 + current_igv_rate())
        ) * instance.demand


@receiver([post_save, post_delete], sender=PurchaseLine)
def update_purchase_amounts(
    sender: PurchaseLine,
    instance: PurchaseLine,
    **kwargs
) -> None:
    """ Actualizar montos globales """
    # obtener compra actual
    current_purchase = instance.purchase
    # sumar subtotales de todos los <detalles de compra> relacionados
    subtotal = current_purchase.purchase_lines.aggregate(
        Sum("price_subtotal"))
    # obtener <base imponible> sumando los subtotales del detalle
    untaxed = subtotal.get("price_subtotal__sum")
    try:
        # obtener <igv> vigente
        current_igv = Tax.objects.filter(
            abbreviation="igv").latest("effective_from")
    except Tax.DoesNotExist:
        igv_rate = Decimal("0.18")
    else:
        igv_rate = current_igv.percentage
    # calcular <impuesto bruto>
    gross_tax = untaxed * Decimal(igv_rate)
    print("gross_tax:", gross_tax)
    # calcular <total>
    total = untaxed + gross_tax
    print("total:", total)
    # Asignar y guardar
    current_purchase.amount_untaxed = untaxed
    current_purchase.amount_tax = gross_tax
    current_purchase.amount_total = total
    current_purchase.save()
