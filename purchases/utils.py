from django.utils import timezone

from sunqu import utils


def generate_code() -> int:
    """Retorna el código de orden de compra consecutivo como Integer."""
    from .models import Purchase
    last_code = 0
    total_entries = Purchase.objects.filter(
        created_at__year=timezone.now().year
    ).count()
    # si existen registros...
    if total_entries > 0:
        # obtener el ultimo registro basado a la fecha de creación
        last_entry = utils.get_last_entry(Purchase, 'created_at')
        # obtener el código del último registro
        last_code = last_entry.code
    # crear el nuevo código consecutivo
    new_code = last_code + 1
    return new_code


def purchase_amount(purchase_object):
    """Cálculo de montos relacionados a orden de compra."""
    from decimal import Decimal
    from django.db.models import Sum
    subtotal = purchase_object.purchaseline_set.aggregate(
        Sum("price_subtotal"))
    # Base imponible
    untaxed = subtotal.get("price_subtotal__sum")
    # IGV
    # TODO: obtener el porcentaje del igv desde una tabla
    tax = untaxed * Decimal("0.18")
    # Total
    total = untaxed + tax
    return {"untaxed": untaxed, "tax": tax, "total": total}
