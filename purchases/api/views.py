from rest_framework import generics

from ..models import Purchase, PurchaseLine
from . import serializers


class ListCreatePurchase(generics.ListCreateAPIView):
    queryset = Purchase.objects.all()
    serializer_class = serializers.PurchaseSerializer


class RetrieveUpdateDestroyPurchase(generics.RetrieveUpdateDestroyAPIView):
    queryset = Purchase.objects.all()
    serializer_class = serializers.PurchaseSerializer
