from django.contrib.auth import get_user

from rest_framework import serializers

from ..models import (
    Purchase,
    PurchaseLine
)


class PurchaseLineSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        fields = "__all__"
        model = PurchaseLine
        read_only_fields = ("purchase",)


class PurchaseSerializer(serializers.ModelSerializer):
    purchase_lines = PurchaseLineSerializer(many=True)
    created_at = serializers.DateTimeField(required=False)
    code = serializers.IntegerField(required=False)
    attentions = serializers.SerializerMethodField()
    invoices = serializers.SerializerMethodField()

    class Meta:
        extra_kwargs = {
            "code": {"read_only": True},
            "created_at": {"read_only": True},
            "updated_at": {"read_only": True, "required": False},
            "user": {"read_only": True}
        }
        fields = "__all__"
        model = Purchase

    def create(self, validated_data):
        purchase_lines = validated_data.pop("purchase_lines")
        purchase = Purchase.objects.create(
            **validated_data, user=self.context['request'].user)
        for line in purchase_lines:
            PurchaseLine.objects.create(**line, purchase=purchase)
        return purchase

    def update(self, instance, validated_data):
        # Capturamos los "detalles de la compra" en una variable separada
        purchase_lines = validated_data.pop("purchase_lines")

        # Actualizamos los campos de la "compra"
        instance.partner_reference = validated_data.get(
            "partner_reference", instance.partner_reference)
        instance.currency = validated_data.get(
            "currency", instance.currency)
        instance.partner = validated_data.get(
            "partner", instance.partner)
        instance.project = validated_data.get(
            "project", instance.project)
        instance.amount_untaxed = validated_data.get(
            "amount_untaxed", instance.amount_untaxed)
        instance.amount_tax = validated_data.get(
            "amount_tax", instance.amount_tax)
        instance.amount_total = validated_data.get(
            "amount_total", instance.amount_total)
        instance.observation = validated_data.get(
            "observation", instance.observation)
        instance.terms_conditions = validated_data.get(
            "terms_conditions", instance.terms_conditions)
        instance.state = validated_data.get(
            "state", instance.state)
        instance.requisition = validated_data.get(
            "requisition", instance.requisition)
        # Finalmente, guardamos la instancia de "compra"
        instance.save()

        # Contenedor de IDs de los "PurchaseLine" que se mantendrán
        keep_lines = []

        # Items existentes actualmente en "instance"
        existing_ids = [line.id for line in instance.purchase_lines.all()]

        # iteramos por los "PurchaseLine" del request
        for line in purchase_lines:
            # si existe la clave "id" es una modificación
            if "id" in line.keys():
                # modificar sólo si el "id" existe, sino ignorar
                try:
                    pl = PurchaseLine.objects.get(id=line.get("id", None))
                except PurchaseLine.DoesNotExist:
                    continue
                else:  # si no hay errores...
                    # actualizar todos los campos del PurchaseLine
                    pl.external_code = line.get(
                        "external_code", pl.external_code)
                    pl.demand = line.get("demand", pl.demand)
                    pl.uom = line.get("uom", pl.uom)
                    pl.thing = line.get("thing", pl.thing)
                    pl.price_unit = line.get("price_unit", pl.price_unit)
                    pl.igv_included = line.get("igv_included", pl.igv_included)
                    pl.quantity_received = line.get(
                        "quantity_received", pl.quantity_received)
                    pl.quantity_billed = line.get(
                        "quantity_billed", pl.quantity_billed)
                    pl.expense_type = line.get("expense_type", pl.expense_type)
                    # guardar "linea"
                    pl.save()
                    # agregar a lista por mantener
                    keep_lines.append(pl.id)
            # si no existe el "id" es un registro nuevo
            else:
                # crear "linea"
                pl = PurchaseLine.objects.create(**line, purchase=instance)
                # agregar a lista por mantener
                keep_lines.append(pl.id)

        # eliminando items de la compra
        for line in instance.purchase_lines.all():
            if line.id not in keep_lines:
                line.delete()

        return Purchase.objects.get(pk=instance.pk)

    def get_attentions(self, obj) -> int:
        """Cantidad de atenciones relacionadas a orden de compra"""
        attentions = obj.moves.count()
        return 0

    def get_invoices(self, obj) -> int:
        """Cantidad de comprobantes relacionados a orden de compra"""
        invoices = obj.invoices.count()
        return 0
