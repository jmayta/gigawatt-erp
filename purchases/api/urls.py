from django.urls import path

from . import views


urlpatterns = [
    path("", views.ListCreatePurchase.as_view(), name="purchase-list"),
    path(
        "<int:pk>/",
        views.RetrieveUpdateDestroyPurchase.as_view(),
        name="purchase-detail"
    ),
]
