from django.urls import path

from . import views
from .views import PurchaseList


app_name = "purchases"

urlpatterns = [
    path("", PurchaseList.as_view(), name="list"),
    path("create/", views.PurchaseCreate.as_view(), name="create"),
    path("<int:pk>/", views.PurchaseDetail.as_view(), name="detail"),
    path("<int:pk>/invoices/", views.InvoiceList.as_view(), name="invoices"),
    path("<int:pk>/moves/", views.MoveList.as_view(), name="moves"),
]

