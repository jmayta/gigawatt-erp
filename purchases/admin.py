from django.contrib import admin
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse

from django_summernote.admin import SummernoteModelAdmin
from import_export.admin import ImportExportModelAdmin

from .forms import PurchaseForm
from .models import Purchase, PurchaseLine


class PurchaseLineInLine(admin.TabularInline):
    model = PurchaseLine
    exclude = (
        'quantity_received',
        'quantity_billed',
        'taxes',
    )

    readonly_fields = (
        'price_subtotal',
    )


class PurchaseAdmin(ImportExportModelAdmin, SummernoteModelAdmin):
    summernote_fields = ("terms_conditions",)

    form = PurchaseForm

    exclude = (
        "user",
    )

    readonly_fields = (
        'amount_untaxed',
        'amount_tax',
        'amount_total',
        'created_at',
        'updated_at',
    )

    inlines = (
        PurchaseLineInLine,
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        from partners.models import Partner
        if db_field.name == "partner":
            kwargs["queryset"] = Partner.objects.filter(
                is_provider=True
            ).filter(
                is_active=True
            )
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def response_add(self, request, obj, post_url_continue=None):
        return redirect("purchases:list")

    def response_change(self, request, obj):
        return redirect("purchases:detail", obj.pk)

    def save_model(self, request, obj, form, change):
        from .utils import generate_code
        if not change:  # si no es una modificación
            # generar código
            obj.code = generate_code()
            # adjuntar autor (usuario actual)
            obj.user = request.user
        super().save_model(request, obj, form, change)


class PurchaseLineAdmin(ImportExportModelAdmin):
    pass


admin.site.register(Purchase, PurchaseAdmin)
