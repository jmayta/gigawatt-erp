from django.apps import apps
from django.db.models import Count
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView


from .models import Purchase
from invoices.models import Invoice
from moves.models import Move
from projects.models import Project


class PurchaseList(LoginRequiredMixin, ListView):
    model = Purchase

    def get_queryset(self):
        if apps.is_installed("moves"):
            queryset = Purchase.objects.annotate(
                moves_count=Count("moves")
            ).annotate(
                invoices_count=Count("invoices")
            ).order_by("-code")

        print("<Moves> isn't installed.")

        if apps.is_installed("invoices"):
            queryset = Purchase.objects.annotate(
                moves_count=Count("moves")
            ).annotate(
                invoices_count=Count("invoices")
            ).order_by("-code")

        print("<Invoices> isn't installed.")

        queryset = Purchase.objects.order_by("-code")
        return queryset


# TODO: Generar "compra" con formulario via API
class PurchaseCreate(TemplateView):
    template_name = "purchases/purchase_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["projects"] = Project.objects.all()
        return context


class PurchaseDetail(DetailView):
    model = Purchase

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Cantidad de atenciones (guias de remisión)
        context["moves_count"] = self.object.moves.count()
        # Cantidad de comprobantes
        context["invoices_count"] = self.object.invoices.count()
        return context

    def get_queryset(self):
        queryset = Purchase.objects.select_related(
            "contact",
            "currency",
            "vendor",
            "project",
            "requisition",
            "author"
        ).prefetch_related(
            "purchase_lines"
        ).all()
        return queryset


class MoveList(ListView):
    model = Move

    def get_queryset(self):
        queryset = Move.objects.filter(purchase=self.kwargs.get("pk"))
        return queryset

    def get_template_names(self):
        return "purchases/purchase_moves_list.html"


class InvoiceList(ListView):
    model = Invoice

    def get_queryset(self):
        queryset = Invoice.objects.filter(purchase=self.kwargs.get("pk"))
        return queryset

    def get_template_names(self):
        return "purchases/purchase_invoice_list.html"
