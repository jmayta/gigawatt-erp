from decimal import Decimal

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext as _

from contacts.models import Contact
from partners.models import Partner
from projects.models import Project
from quotations.models import Quotation
from requisitions.models import Requisition
from sunat.models import Tax
from sunqu.behaviors import Timestampable
from things.models import Thing
from universal.models import Currency, UnitOfMeasurement


User = get_user_model()


class Purchase(Timestampable, models.Model):
    STATE_CREATED = "cr"  # creado
    STATE_OPENED = "op"  # abierto
    STATE_CLOSED = "cl"  # cerrado
    STATE_CANCELED = "cn"  # cancelado
    STATE_CHOICES = (
        (STATE_CREATED, _("created")),
        (STATE_OPENED, _("opened")),
        (STATE_CLOSED, _("closed")),
        (STATE_CANCELED, _("canceled")),
    )
    # Código
    code = models.PositiveIntegerField(
        _("code"),
        blank=True,
        unique_for_year="created_at"
    )
    # Usuario / Autor
    author = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        verbose_name=_("author")
    )
    # Cotizaciones
    quotations = models.ManyToManyField(
        Quotation,
        blank=True
    )
    # Moneda
    currency = models.ForeignKey(
        Currency,
        on_delete=models.PROTECT,
        verbose_name=_("currency")
    )
    # Socio de negocio / Proveedor
    vendor = models.ForeignKey(
        Partner,
        on_delete=models.PROTECT,
        related_name="purchases",
        verbose_name=_("vendor")
    )
    # Contacto de proveedor
    contact = models.ForeignKey(
        Contact,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="purchases",
        verbose_name=_("contact")
    )
    # Proyecto
    project = models.ForeignKey(
        Project,
        on_delete=models.PROTECT,
        related_name='purchases',
        verbose_name=_("project")
    )
    # Base imponible
    untaxed_amount = models.DecimalField(
        _("untaxed amount"),
        max_digits=13,
        decimal_places=2,
        default=0.000
    )
    # Monto Impuesto
    tax_amount = models.DecimalField(
        _("tax amount"),
        max_digits=13,
        decimal_places=2,
        default=0.000
    )
    # Monto total
    total_amount = models.DecimalField(
        _("total amount"),
        max_digits=13,
        decimal_places=2,
        default=0.000
    )
    # Observación
    observation = models.TextField(_("observation"), default="")
    # Términos y condiciones
    terms_conditions = models.TextField(_("terms and conditions"), default="")
    # Estado
    state = models.CharField(
        _("state"),
        max_length=2,
        choices=STATE_CHOICES,
        default=STATE_CREATED
    )
    # Requerimiento
    requisition = models.ForeignKey(
        Requisition,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="purchases",
        verbose_name=_("requisition")
    )

    class Meta:
        verbose_name = _("purchase")
        verbose_name_plural = _("purchases")
        ordering = ["-code"]

    def __str__(self):
        from sunqu.utils import codify
        string_code = codify(self.code, 5)
        created_year = self.created_at.strftime("%y")
        return f"{string_code}-{created_year}"

    def save(self, *args, **kwargs):
        # nothing to do
        return super().save(*args, **kwargs)

    def set_untaxed_amount(self):
        pass

    def set_tax_amount(self):
        pass

    def set_total_amount(self):
        pass


class PurchaseThing(models.Model):
    # Compra
    purchase = models.ForeignKey(
        Purchase,
        on_delete=models.CASCADE,
        related_name="purchase_lines"
    )
    # Código externo del artículo
    external_code = models.CharField(
        _("external code"),
        blank=True,
        default="",
        max_length=80
    )
    # Demanda
    demand = models.FloatField(_("demand"), default=0.00)
    # Unidad de medida
    uom = models.ForeignKey(
        UnitOfMeasurement,
        on_delete=models.PROTECT,
        verbose_name=_("unit of measurement")
    )
    # Artículo
    thing = models.ForeignKey(
        Thing,
        on_delete=models.PROTECT,
        related_name="purchases",
        verbose_name=_("thing")
    )
    # Precio Unitario
    unit_price = models.DecimalField(
        _("unit price"),
        decimal_places=4,
        default=Decimal(0.00),
        max_digits=15
    )
    # Impuestos (m2m)
    taxes = models.ManyToManyField(
        Tax,
        blank=True
    )
    # Valor unitario
    unit_value = models.DecimalField(
        _("unit value"),
        decimal_places=4,
        default=Decimal(0.00),
        max_digits=15,
    )
    # Subtotal
    subtotal = models.DecimalField(
        _("subtotal"),
        decimal_places=4,
        default=0.000,
        max_digits=15
    )
    # Cantidad Recibida
    quantity_received = models.DecimalField(
        _("received quantity"),
        max_digits=5,
        decimal_places=2,
        default=0.00
    )
    # Cantidad Facturada
    quantity_billed = models.DecimalField(
        _("billed quantity"),
        max_digits=5,
        decimal_places=2,
        default=0.00
    )
    # Tipo de Gasto
    # expense_type = models.ForeignKey(
    #     ExpenseType,
    #     null=True,
    #     on_delete=models.SET_NULL,
    #     verbose_name=_("expense type")
    # )

    def __str__(self):
        return "{0}".format(self.thing)
