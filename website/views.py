from django.views.generic.base import TemplateView
from django.shortcuts import render


class LandingPageView(TemplateView):
    template_name = "website/index.html"
