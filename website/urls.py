from django.urls import path

from . import views


app_name = "website"

urlpatterns = [
    path("", views.LandingPageView.as_view(), name="landing"),
]
