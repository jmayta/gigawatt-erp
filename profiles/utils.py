def profile_image_path(instance, filename):
    extension = filename.split(".")[-1]  # ej: 'png'
    return f"profiles/{instance.user.username}.{extension}"
