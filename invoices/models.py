from decimal import Decimal

from django.db import models
from django.utils.translation import gettext_lazy as _

from cashflow.models import Flow
from moves.models import Move
from partners.models import Partner
from orders.models import Order
from sunqu.behaviors import Timestampable
from universal.models import Currency


class Bill(Flow):
    # Documento (escaneado)
    document = models.FileField(upload_to="flow/bills/%Y/")
    # Nro de serie
    serial_numeration = models.CharField(
        _("serial number"),
        max_length=13
    )
    # Fecha de vencimiento
    due_date = models.DateField(_("due date"), blank=True, null=True)
    # Orden (compra/servicio)
    order = models.ForeignKey(
        Order,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("order")
    )
    # Documento de movimiento (GR)
    move = models.ForeignKey(
        Move,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("move")
    )


class Invoice(Bill):
    # Documento (escaneado)
    document = models.FileField(upload_to="flow/invoices/%Y/")
    # Total Valor de Venta (NO inc. impuestos)
    amount_line_extension = models.DecimalField(
        decimal_places=2,
        default=Decimal(0.00),
        max_digits=13
    )
    # Total Impuesto (IGV)
    amount_tax_total = models.DecimalField(
        decimal_places=2,
        default=Decimal(0.00),
        max_digits=12
    )
    # Total Precio de Venta (Inc. impuestos)
    amount_tax_inclusive = models.DecimalField(
        decimal_places=2,
        default=Decimal(0.00),
        max_digits=13
    )

    def __str__(self):
        return "{0}".format(self.serial_numeration)
