from django.contrib import admin

from .models import Invoice


# class InvoiceAdmin(admin.ModelAdmin):
#     list_display = (
#         "numeration",
#         "partner_business_name",
#         "issue_date",
#         "due_date",
#         "currency",
#         "purchase",
#         "amount_tax_inclusive",
#     )
#     search_fields = (
#         "numeration",
#         "amount_total",
#         "partner__business_name",
#     )
#
#     def partner_business_name(self, obj):
#         return obj.partner.business_name


admin.site.register(Invoice)
