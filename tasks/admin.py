from django.contrib import admin

from .models import Task, TaskReview


class TaskReviewAdmin(admin.ModelAdmin):
    model = TaskReview
    exclude = (
        "created_at",
        "updated_at",
    )
    readonly_fields = (
        "description",
        "start_date",
        "end_date",
    )

admin.site.register(Task)
admin.site.register(TaskReview, TaskReviewAdmin)
