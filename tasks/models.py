from django.db import models


class Task(models.Model):
    description = models.CharField(max_length=255)
    start_date = models.DateField()
    end_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{0}".format(self.description)


class TaskReview(Task):
    class Meta:
        proxy = True
