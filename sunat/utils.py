from decimal import Decimal


def current_igv_rate():
    from .models import Tax
    IGV_ABBREVIATION = "igv"
    latest_igv_rate = 0
    try:
        latest_igv_rate = Tax.objects.filter(
            abbreviation=IGV_ABBREVIATION
        ).latest("effective_from")
    except Tax.DoesNotExist:
        latest_igv_rate = Decimal("0.18")
        return latest_igv_rate
    else:
        return latest_igv_rate.percentage
