from decimal import Decimal

from django.utils.translation import gettext_lazy as _
from django.db import models

from sunqu.behaviors import Timestampable


class Tax(Timestampable, models.Model):
    """ Impuesto """
    # Descripción/nombre
    description = models.CharField(_("description"), max_length=255)
    # Abreviación
    abbreviation = models.CharField(
        _("abbreviation"),
        blank=True,
        default="",
        max_length=50
    )
    # Porcentaje
    rate = models.DecimalField(
        _("rate"),
        decimal_places=2,
        default=Decimal(0.00),
        max_digits=3
    )
    # Vigente desde
    effective_from = models.DateField(_("effective from"))

    class Meta:
        ordering = ("description", "-effective_from",)
        verbose_name = _("tax")
        verbose_name_plural = _("taxes")

    def __str__(self):
        return f"{self.abbreviation} ({self.rate})"


# class OperationType(models.Model):
#     """
#     Tipos de operación según <Tabla 17> de acuerdo a resolución
#     Nº 340-2017/SUNAT
#     https://kutt.it/dW5B9m
#     """
#     # Código
#     code = models.CharField(max_length=2, unique=True)
#     # Descripción
#     description = models.CharField(max_length=254, unique=True)

#     class Meta:
#         ordering = ["code"]

#     def __str__(self):
#         return "{0}".format(self.description)


# class InvoiceType(models.Model):
#     """
#     Tipos de comprobante <Tabla 01>
#     https://kutt.it/dW5B9m
#     """
#     # Código
#     code = models.CharField(max_length=2, unique=True)
#     # Descripción
#     description = models.CharField(max_length=254, unique=True)

#     class Meta:
#         ordering = ["code"]

#     def __str__(self):
#         return "{0}".format(self.description)


# class DocumentType(models.Model):
#     """
#     Tipos de documento de indentidad <Tabla 06>
#     https://kutt.it/dW5B9m
#     """
#     code = models.CharField(max_length=2)
#     description = models.CharField(max_length=150)
#     abbreviation = models.CharField(max_length=20)

#     def __str__(self):
#         return "{0}".format(self.description)
