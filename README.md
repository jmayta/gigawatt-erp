# Gigawatt ERP

Sistema de gestión de recursos empresariales (ERP) para la empresa Gigawatt S.A.C a.k.a. Gigawatt Ingenieros.

### Templates

##### templates/include/main_navbar.html

El `main_navbar` muestra los macro módulos con los que cuenta el sistema. Éstos pueden ser por ejemplo el módulo de Logística y Abastecimiento, Recursos Humanos, Contabilidad, etc.

##### templates/include/user_menu.html

El `user_menu` muestra algunas características del usuario logueado. Se encuentra ubicado sobre el `sidebar_menu`.


### Dashboard

El dashboard contiene la lista de todos los módulos que puede utilizar.

### Módulos:
+ Users
+ Profiles
    + Profile
+ Universal
    + Country
    + Currency
    + Exchange Rate
    + Unit Of Measurement
+ Contacts
    + Contact
+ Partners
    + Partner
    + Directory (m2m Contacts)
+ Projects
    + Project
    + Membership
+ Things
    + Thing
    + Composition
    + Identification
+ Maintenances
    + Maintenance
+ Requisitions
    + Requisition
    + Requisition Thing
+ Quotations
    + Quotation
    + Quotation Thing
+ Sunat
    + Tax
+ Warehouses
    + Warehouse
    + Storekeeper
+ Stocks
    + Stock
+ Moves
    + Move
    + Move Thing
+ Orders
    + Order
    + Order Thing


### Proyectos

+ Socios de negocio
+ Colaboradores

#### main_navigation


###### links


## Exportar / Importar

Orden de importación
1. Universal
    + Paises (countries)
    + Monedas (currencies)
    + Unidades de medida (units of measurement)
2. Socios de Negocio (partners)
3. Artículos (things)


## Ideas para implementar (upgrades)

- [ ] Se debe registrar (en algún lugar) la `moneda` con la que se trabajara de manera predeterminada en determinada `compañia`.
- Se debe implementar el módulo de `mensajes/observaciones` para guardar comentarios, observaciones y mensajes de todos los otros módulos.
