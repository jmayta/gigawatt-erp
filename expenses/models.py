from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models here.
class ExpenseType(models.Model):
    """Tipo de gasto"""
    # Nombre
    name = models.CharField(_("name"), max_length=255)
    # Tipo de Gasto Padre
    parent = models.ForeignKey(
        "self",
        blank=True,
        help_text=_("Parent expense type"),
        null=True,
        on_delete=models.CASCADE,
        related_name="children",
        verbose_name=_("parent"),
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["name"],
                name="unique_xtype_name"
            )
        ]

    def __str__(self):
        return '{0}'.format(self.name)
