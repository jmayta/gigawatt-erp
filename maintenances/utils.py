def certificate_path(instance, filename):
    extension = filename.split(".")[-1]  # ej: 'pdf'
    # código de objeto
    thing_code = instance.identifiable.thing.code
    # nro de serie de instancia de objeto
    serial_number = instance.identifiable.serial_number
    # nro de certificado de calibración/mantenimiento
    certificate_number = instance.certificate_reference

    # nombre de archivo -> codigoArticulo_serie_numeroDeCertificado.pdf
    filename = f"{thing_code}_{serial_number}_{certificate_number}"
    return f"maintenances/{filename}.{extension}"
