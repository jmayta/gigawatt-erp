from django.utils.translation import gettext_lazy as _
from django.db import models

from .utils import certificate_path
from partners.models import Partner
from things.models import Identifiable


class Maintenance(models.Model):
    # referencia de documento / nro de certificado
    certificate_reference = models.CharField(
        _("document reference"),
        max_length=100
    )
    # certificado
    # TODO: Independizar los certificados como módulo
    certificate = models.FileField(
        _("certificate"),
        blank=True,
        null=True,
        upload_to=certificate_path
    )
    # fecha de mantenimiento
    maintenance_date = models.DateField(_("maintenance date"))
    # gestor de mantenimiento (proveedor)
    manager = models.ForeignKey(
        Partner,
        on_delete=models.PROTECT,
        related_name="maintenances",
        verbose_name=_("manager")
    )
    # objeto identificable / objeto con serie
    identifiable = models.ForeignKey(
        Identifiable,
        on_delete=models.CASCADE,
        related_name="maintenances",
        verbose_name=_("identifiable")
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["certificate_reference", "identifiable"],
                name="unique_certificate_by_serial"
            )
        ]
        verbose_name = _("maintenance")
        verbose_name_plural = _("maintenances")

    def __str__(self):
        return f"{self.identifiable} -> {self.maintenance_date}"
