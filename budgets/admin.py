from django.contrib import admin

from .models import Budget, BudgetLine


class BudgetLineInLine(admin.TabularInline):
    model = BudgetLine

    exclude = (
        "created_at",
        "updated_at",
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        from universal.models import UnitOfMeasurement
        if db_field.name == "uom":
            kwargs["queryset"] = UnitOfMeasurement.objects.order_by("name")
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class BudgetAdmin(admin.ModelAdmin):
    inlines = (
        BudgetLineInLine,
    )

    exclude = (
        "created_at",
        "updated_at",
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        from projects import utils as project_utils
        if db_field.name == "project":
            kwargs["queryset"] = project_utils.active_projects()
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Budget, BudgetAdmin)
