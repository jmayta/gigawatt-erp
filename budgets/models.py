from decimal import Decimal

from django.db import models

from projects.models import Project
from sunqu.behaviors import Timestampable
from things.models import Thing
from universal.models import (
    Currency,
    UnitOfMeasurement
)


class Budget(Timestampable, models.Model):
    # Descripción
    description = models.CharField(
        blank=True,
        default="",
        max_length=255
    )
    # Proyecto
    project = models.OneToOneField(
        Project,
        on_delete=models.PROTECT
    )
    # Moneda
    currency = models.ForeignKey(
        Currency,
        null=True,
        on_delete=models.SET_NULL
    )

    class Meta:
        verbose_name = "presupuesto"
        verbose_name_plural = "presupuestos"

    def __str__(self):
        return f"{self.project}"


class BudgetThing(Timestampable, models.Model):
    # Presupuesto
    budget = models.ForeignKey(
        Budget,
        on_delete=models.CASCADE,
        related_name="lines"
    )
    # Descripción del artículo (cliente)
    customer_description = models.CharField(
        blank=True,
        default="",
        max_length=250
    )
    # Artículo
    thing = models.ForeignKey(
        Thing,
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )
    # Cantidad
    quantity = models.DecimalField(
        decimal_places=2,
        default=Decimal("0.00"),
        max_digits=12
    )
    # Unidad de Medida
    uom = models.ForeignKey(
        UnitOfMeasurement,
        null=True,
        on_delete=models.SET_NULL
    )
    # Precio Unitario
    unit_price = models.DecimalField(
        decimal_places=3,
        default=Decimal("0.00"),
        max_digits=13
    )

    class Meta:
        unique_together = ["budget", "thing"]
        verbose_name = "detalle de presupuesto"
        verbose_name_plural = "detalles de presupuesto"
