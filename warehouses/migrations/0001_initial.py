# Generated by Django 2.2.8 on 2020-02-19 05:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('projects', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Storekeeper',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, null=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(blank=True, null=True, verbose_name='updated at')),
                ('is_active', models.BooleanField(default=True, verbose_name='is active')),
                ('joining_date', models.DateField(verbose_name='joining date')),
                ('leaving_date', models.DateField(blank=True, null=True, verbose_name='leaving date')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='user')),
            ],
            options={
                'verbose_name': 'storekeeper',
                'verbose_name_plural': 'storekeepers',
            },
        ),
        migrations.CreateModel(
            name='Warehouse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, null=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(blank=True, null=True, verbose_name='updated at')),
                ('name', models.CharField(max_length=254, verbose_name='name')),
                ('is_virtual', models.BooleanField(default=False, verbose_name='is virtual')),
                ('address', models.TextField(blank=True, default='', verbose_name='address')),
                ('address_second_line', models.TextField(blank=True, default='', verbose_name='address second line')),
                ('map_url', models.TextField(blank=True, default='', verbose_name='map url')),
                ('location', models.CharField(blank=True, default='', help_text='district, city, state', max_length=254, verbose_name='location')),
                ('is_active', models.BooleanField(default=True, verbose_name='is active')),
                ('project', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='warehouses', to='projects.Project', verbose_name='project')),
                ('storekeepers', models.ManyToManyField(through='warehouses.Storekeeper', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'warehouse',
                'verbose_name_plural': 'warehouses',
                'ordering': ['name'],
            },
        ),
        migrations.AddField(
            model_name='storekeeper',
            name='warehouse',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='warehouses.Warehouse', verbose_name='warehouse'),
        ),
        migrations.AddConstraint(
            model_name='warehouse',
            constraint=models.UniqueConstraint(fields=('name', 'project'), name='unique_project_warehouse'),
        ),
    ]
