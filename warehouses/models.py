from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _

from partners.models import Partner
from projects.models import Project

from sunqu.behaviors import Timestampable


User = get_user_model()


class Warehouse(Timestampable, models.Model):
    # Nombre
    name = models.CharField(_("name"), max_length=254)
    # Es virtual
    is_virtual = models.BooleanField(_("is virtual"), default=False)
    # Dirección
    address = models.TextField(_("address"), blank=True, default="")
    # Dirección Segunda Linea
    address_second_line = models.TextField(
        _("address second line"),
        blank=True,
        default=""
    )
    # Maps Link
    map_url = models.TextField(_("map url"), blank=True, default="")
    # Ubicación (ubigeo)
    location = models.CharField(
        _("location"),
        blank=True,
        default="",
        help_text=_("district, city, state"),
        max_length=254,
    )
    # Proyecto
    project = models.ForeignKey(
        Project,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="warehouses",
        verbose_name=_("project")
    )
    # ¿Almacén activo?
    is_active = models.BooleanField(_("is active"), default=True)
    # Responsables de almacén
    storekeepers = models.ManyToManyField(User, through="Storekeeper")

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["name", "project"],
                name="unique_project_warehouse"
            )
        ]
        ordering = ["name"]
        verbose_name = _("warehouse")
        verbose_name_plural = _("warehouses")

    def __str__(self):
        if self.project:
            return f"{self.project} / {self.name}"
        else:
            return f"{self.name}"


class Storekeeper(Timestampable, models.Model):
    # Almacén
    warehouse = models.ForeignKey(
        Warehouse,
        on_delete=models.PROTECT,
        verbose_name=_("warehouse")
    )
    # Usuario
    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        verbose_name=_("user")
    )
    # Está activo
    is_active = models.BooleanField(_("is active"), default=True)
    # Fecha de ingreso
    joining_date = models.DateField(_("joining date"))
    # Fecha de salida
    leaving_date = models.DateField(_("leaving date"), blank=True, null=True)

    class Meta:
        verbose_name = _("storekeeper")
        verbose_name_plural = _("storekeepers")

    def __str__(self):
        return f"{self.warehouse} / {self.user}"
