from django.urls import path

from . import views


app_name = "warehouses"

urlpatterns = [
    path("", views.WarehouseListView.as_view(), name="list"),
    path("<int:pk>", views.WarehouseDetailView.as_view(), name="detail"),
    path(
        "<int:pk>/stock",
        views.WarehouseStockListView.as_view(),
        name="stock"
    ),
    path("<int:pk>/shippings", views.shippings),
    path("<int:pk>/arrivals", views.arrivals),
]
