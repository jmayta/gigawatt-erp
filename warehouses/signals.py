from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from projects.models import Project
from warehouses.models import Warehouse


@receiver(post_save, sender=Project)
def create_project_warehouse(sender, instance, created, **kwargs):
    if instance and created is True:
        Warehouse.objects.create(name=instance.alias, project=instance)
