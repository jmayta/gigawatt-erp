from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class WarehousesConfig(AppConfig):
    name = 'warehouses'
    verbose_name = _("warehouses")
