from django.contrib import admin

from .models import Warehouse, Storekeeper


class StorekeeperInLine(admin.TabularInline):
    model = Storekeeper
    exclude = (
        'created_at',
        'updated_at',
    )


class WarehouseAdmin(admin.ModelAdmin):
    exclude = (
        'created_at',
        'updated_at',
    )
    inlines = [
        StorekeeperInLine,
    ]

    list_display = (
        "name",
        "address",
        "location",
        "is_active",
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        from projects.utils import active_projects
        if db_field.name == "project":
            kwargs["queryset"] = active_projects()
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Storekeeper)
admin.site.register(Warehouse, WarehouseAdmin)
