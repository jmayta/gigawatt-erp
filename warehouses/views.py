from django.db.models import Count
from django.shortcuts import render, get_object_or_404
from django.views import generic


from .models import Storekeeper, Warehouse
from moves.models import Move
from stocks.models import Stock


class WarehouseListView(generic.ListView):
    model = Warehouse

    def get_queryset(self):
        queryset = Warehouse.objects.annotate(Count("stock"))
        return queryset


class WarehouseDetailView(generic.DetailView):
    model = Warehouse

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["storekeepers"] = Storekeeper.objects.filter(
            warehouse=self.object, is_active=True)
        return context


class WarehouseStockListView(generic.ListView):
    model = Stock
    template_name = "warehouses/warehouse_stock.html"

    def get_queryset(self):
        warehouse = self.kwargs.get("pk")
        return Stock.objects.filter(warehouse=warehouse)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["warehouse"] = get_object_or_404(
            Warehouse, pk=self.kwargs.get("pk")
        )
        return context


def shippings(request, pk):
    shippings = Move.objects.filter(origin=pk)
    return render(request, 'warehouses/shipping_list.html', {
        'object_list': shippings
    })


def arrivals(request, pk):
    arrivals = Move.objects.filter(destination=pk)
    return render(request, 'warehouses/arrival_list.html', {
        'object_list': arrivals
    })
