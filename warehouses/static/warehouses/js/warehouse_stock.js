document.addEventListener('DOMContentLoaded', function() {
    $('.datatable-responsive-column-controlled').DataTable({
        responsive: {
            details: {
                type: 'column'
            }
        },
        columnDefs: [
            {
                className: 'control',
                orderable: false,
                targets: 0,
                width: "30px",
                defaultContent: ""
            }
        ],
        order: [1, 'asc']
    });
});
