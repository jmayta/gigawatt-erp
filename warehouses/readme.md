## Warehouses / Almacenes

Se pueden crear almacenes para socios de negocio y para clientes internos (proyectos).

### Base de datos

Disponible [aquí](https://dbdiagram.io/d/5ced36851f6a891a6a657ca4).

### Campos

- **Created at** / *Fecha de creación*:
    <br>Automático

- **Updated at** / *Fecha de actualización*:
    <br>Automático

- **Name** / *nombre*:
    <br>El nombre deberá ser acorde al proyecto, lugar donde se encuentre ubicado o al socio que pertenezca. Por ejemplo:
    
    #### Caso 1:

    *En la ciudad de Huancayo tenemos tres almacenes, situados en distintos puntos de la ciudad.*
    1. *Huancayo (Sede Principal)*
    2. *Huari (Casa de Densy)*
    3. *El Tambo (Casa de Eder)*
    
    *Entonces los nombres de los almacenes pueden estar determinados por los nombres de los distritos, es decir -> Almacén Central, Almacén Huari, Almacén El Tambo*

    #### Caso 2:
    
    *Estamos ejecutando el proyecto 426 que cuenta con cuatro subproyectos (426-I, 426-II, 426-III, 426-IV), los cuales están distribuídos a lo largo de la región Ayacucho, para este caso existen dos posibilidades para nombrar a los almacenes:*
    1. *Por subproyecto*
        <br>*Almacén 426 (Principal), Almacén 426-I, Almacén 426-II, Almacén 426-III, Almacén 426-IV*
    2. *Por Zona de acción*
        <br>*Almacén Huamanga (426), Almacén Huamanga Rural (426-I), Almacén Huanta (426-II), Almacén Cangayo (426-III), Almacén San Francisco (426-IV)*
    
    #### Caso 3:

    *En el caso de que vayamos a registrar movimientos de mercancías con nuestros proveedores y/o clientes, al crear el(los) almacén(es) debemos nombrarlo(s) con la misma lógica del Caso 1 o Caso 2.*

- **Address** / *Dirección*:
    <br>Dirección del almacén (no siempre es la dirección fiscal, tener cuidado).

- **Address 2** / *Dirección línea 2*:
    <br>Línea 2, detalle adicional relacionada a la dirección, referencia, etc.

- **Location** / *Ubigeo*:
    <br>Ubigeo con forma *Distrito / Provincia / Departamento*

- **Project** / *Proyecto (cliente interno)*:
    <br>*[Opcional]* Se seleccionará en caso se esté registrando un almacén para un determinado proyecto (cliente interno).

- **Partner** / *Socio de negocio*:
    <br>*[Opcional]* Se seleccionará en caso se esté registrando un almacén para un determinado Socio de Negocio (cliente externo o proveedor).

- **Is Active** / *¿Está activo?*:
    <br>Estado del almacén. En caso se desactive el almacén no aparecerá en las listas desplegables de los otros módulos.

> **Advertencia:** No se debe seleccionar conjuntamente el Proyecto y el Socio de Negocio ya que generaría inconsistencias en los registros.
