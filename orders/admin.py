from django.contrib import admin
from django.shortcuts import redirect

from .models import Order, OrderThing


class OrderThingInline(admin.TabularInline):
    model = OrderThing


class OrderAdmin(admin.ModelAdmin):
    exclude = (
        "created_at",
        "updated_at",
    )
    inlines = [
        OrderThingInline,
    ]

    model = Order

    def response_add(self, request, obj, post_url_continue=None):
        return redirect("orders:list")

    def response_change(self, request, obj):
        return redirect("orders:detail", obj.id)

    def save_model(self, request, obj, form, change):
        from sunqu.utils import generate_integer_code
        # si no es una modificación
        if not change:
            # generar nuevo código
            new_code = generate_integer_code(Order, "code", "created_at")
            # asignar nuevo código
            obj.code = new_code
            # autor = usuario actual
            obj.author = request.user
        return super().save_model(request, obj, form, change)


admin.site.register(Order, OrderAdmin)
