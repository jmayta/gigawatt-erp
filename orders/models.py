from decimal import Decimal

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model

from contacts.models import Contact
from partners.models import Partner
from projects.models import Project
from quotations.models import Quotation
from requisitions.models import Requisition
from sunat.models import Tax
from sunqu.utils import codify
from sunqu.behaviors import Timestampable
from things.models import Thing
from universal.models import Currency, UnitOfMeasurement


User = get_user_model()


class Order(Timestampable, models.Model):
    STATE_CREATED = "cr"  # creado
    STATE_OPENED = "op"  # abierto
    STATE_CLOSED = "cl"  # cerrado
    STATE_CANCELED = "cn"  # cancelado
    STATE_CHOICES = (
        (STATE_CREATED, _("created")),
        (STATE_OPENED, _("opened")),
        (STATE_CLOSED, _("closed")),
        (STATE_CANCELED, _("canceled")),
    )
    # Código
    code = models.PositiveIntegerField(
        blank=True,
        unique_for_year="created_at"
    )
    # Autor
    author = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        verbose_name=_("author")
    )
    # Cotizaciones
    quotations = models.ManyToManyField(
        Quotation,
        blank=True,
        related_name="orders",
        verbose_name=_("quotations")
    )
    # Moneda
    currency = models.ForeignKey(
        Currency,
        on_delete=models.PROTECT,
        verbose_name=_("currency")
    )
    # Proveedor
    vendor = models.ForeignKey(
        Partner,
        on_delete=models.PROTECT,
        related_name="orders",
        verbose_name=_("vendor")
    )
    # Contacto
    contact = models.ForeignKey(
        Contact,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("contact")
    )
    # Proyecto
    project = models.ForeignKey(
        Project,
        on_delete=models.PROTECT,
        related_name="orders",
        verbose_name=_("project")
    )
    # Monto sin impuesto
    amount_untaxed = models.DecimalField(
        _("untaxed amount"),
        decimal_places=2,
        default=Decimal(0.00),
        max_digits=13
    )
    # Monto con impuesto
    amount_tax = models.DecimalField(
        _("tax amount"),
        decimal_places=2,
        default=Decimal(0.00),
        max_digits=13
    )
    # Monto total
    amount_total = models.DecimalField(
        _("total amount"),
        decimal_places=2,
        default=Decimal(0.00),
        max_digits=13,
    )
    # Observaciones
    observations = models.TextField(
        _("observations"),
        blank=True,
        default=""
    )
    # Términos y condiciones
    terms_conditions = models.TextField(
        _("terms & conditions"),
        blank=True,
        default=""
    )
    # Estado
    state = models.CharField(
        _("state"),
        max_length=2,
        choices=STATE_CHOICES,
        default=STATE_CREATED
    )
    # Requerimiento
    requisition = models.ForeignKey(
        Requisition,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="orders",
        verbose_name=_("requisition")
    )

    def __str__(self):
        return f"{codify(self.code, 5)}"


class OrderThing(models.Model):
    # Orden
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    # Código externo
    external_code = models.CharField(
        _("external code"),
        blank=True,
        default="",
        max_length=50
    )
    # Demanda
    demand = models.FloatField(_("demand"), default=0.00)
    # Unidad de medida
    uom = models.ForeignKey(
        UnitOfMeasurement,
        on_delete=models.PROTECT,
        verbose_name=_("unit of measurement")
    )
    # Artículo
    thing = models.ForeignKey(
        Thing,
        on_delete=models.PROTECT,
        related_name="orders",
        verbose_name=_("thing")
    )
    # Monto sin impuestos
    amount_untaxed = models.DecimalField(
        _("untaxed amount"),
        decimal_places=3,
        default=Decimal(0.000),
        max_digits=14
    )
    # Impuestos
    taxes = models.ManyToManyField(
        Tax,
        blank=True,
        verbose_name=_("taxes")
    )
    # Monto con impuestos
    amount_taxed = models.DecimalField(
        _("taxed amount"),
        decimal_places=3,
        default=Decimal(0.000),
        max_digits=14
    )
    # Subtotal/Valor de compra
    subtotal = models.DecimalField(
        _("value order"),
        decimal_places=3,
        default=Decimal(0.000),
        max_digits=14
    )
    # Cantidad recibida
    quantity_received = models.FloatField(
        _("quantity received"),
        default=0.00
    )
    # Cantidad facturada
    quantity_billed = models.FloatField(
        _("quantity billed"),
        default=0.00
    )
    # TODO: Enlazar a <estructura de costos>
    # expense_type = models.ForeignKey()

    def __str__(self):
        return f"{self.thing}"
