from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from orders.models import Order


class OrderList(ListView):
    model = Order

    def get_queryset(self):
        queryset = Order.objects.select_related("author","project","vendor")
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class OrderDetail(DetailView):
    model = Order
