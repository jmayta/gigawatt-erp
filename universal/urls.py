from django.urls import path, include

from . import views


app_name = "universal"

urlpatterns = [
    path(
        "countries/",
        views.CountryListView.as_view(),
        name="country-list"
    ),
    path(
        "countries/<int:pk>/",
        views.CountryDetailView.as_view(),
        name="country-detail"
    ),
]
