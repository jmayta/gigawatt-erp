from decimal import Decimal
from django.db import models
from django.utils.translation import gettext as _

from sunqu.behaviors import Timestampable


class Country(models.Model):
    """Modelo País"""
    # Nombre
    name = models.CharField(_("name"), max_length=255)
    # Código Alpha-2
    # https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
    alpha_2 = models.CharField(max_length=2)
    # Código Alpha-3
    # https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3
    alpha_3 = models.CharField(max_length=3)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["name"],
                name="unique_country_name"
            ),
            models.UniqueConstraint(
                fields=["alpha_2"],
                name="unique_country_alpha2_code"
            ),
            models.UniqueConstraint(
                fields=["alpha_3"],
                name="unique_country_alpha3_code"
            )
        ]
        verbose_name = _("country")
        verbose_name_plural = _("countries")

    def __str__(self):
        return f"{self.name}"


class Currency(models.Model):
    """Divisa"""
    # Nombre
    name = models.CharField(_("name"), max_length=255)
    # Nombre en plural
    plural_name = models.CharField(
        _("plural name"),
        blank=True,
        default="",
        max_length=255
    )
    # Nombre de decimales (ex: céntimos, centavos)
    decimal_name = models.CharField(
        _("decimal name"),
        blank=True,
        default="",
        max_length=255
    )
    # Código ISO 4217
    # https://es.wikipedia.org/wiki/ISO_4217
    code = models.CharField(
        _("iso code"),
        max_length=3,
        help_text="https://es.wikipedia.org/wiki/ISO_4217"
    )
    # Símbolo (ex: S/ ; $ ; € )
    symbol = models.CharField(
        # Translators: Example abbreviation
        _("symbol"), max_length=5, help_text=_("Ex")+": S/ ; $ ; € ")

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["name"],
                name="unique_currency_name"
            ),
            models.UniqueConstraint(
                fields=["code"],
                name="unique_currency_code"
            )
        ]
        verbose_name = _("currency")
        verbose_name_plural = _("currencies")

    def __str__(self):
        return f"{self.name}"


class ExchangeRate(Timestampable, models.Model):
    """Tasa de Cambio"""
    # Moneda
    currency = models.ForeignKey(
        Currency,
        on_delete=models.CASCADE,
        related_name="xrates",
        verbose_name=_("exchange rate")
    )
    # Tarifa de compra
    buy_rate = models.DecimalField(
        _("buy rate"),
        max_digits=16,
        decimal_places=6,
        default=Decimal(1.000000)
    )
    # Tarifa de venta
    sell_rate = models.DecimalField(
        _("sell rate"),
        max_digits=16,
        decimal_places=6,
        default=Decimal(1.000000)
    )
    # Fecha de vigencia
    effective_date = models.DateField(_("effective date"))

    class Meta:
        verbose_name = _("exchange rate")
        verbose_name_plural = _("exchange rates")

    def __str__(self):
        return f"{self.created_at} {self.currency}"


class UnitOfMeasurement(models.Model):
    """Unidad de medida"""
    # Nombre de la unidad de medida
    name = models.CharField(_("name"), max_length=255)
    # Nombre en plural
    plural_name = models.CharField(_("plural name"), max_length=255)
    # Abreviatura
    abbreviation = models.CharField(_("abbreviation"), max_length=5)
    # Valor numérico ( ej: 1k = 1000 )
    numerical_value = models.FloatField(_("numerical value"), default=1.0)
    # Unidad base
    # (ej: kg (1000) => g (1) ; cm (100) => m (1) ; doc (12) => und (1) )
    base_unit = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="children",
        verbose_name=_("base unit")
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["name"],
                name="unique_uom_name"
            ),
            models.UniqueConstraint(
                fields=["abbreviation"],
                name="unique_uom_abbreviation"
            )
        ]
        ordering = ["name", ]
        verbose_name = _("unit of measurement")
        verbose_name_plural = _("units of measurement")

    def __str__(self):
        return "{0} ({1})".format(self.name, self.abbreviation)
