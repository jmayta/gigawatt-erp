from django.contrib import admin

from import_export.admin import ImportExportModelAdmin

from .models import (
    Country,
    Currency,
    UnitOfMeasurement
)


class UnitMeasureAdmin(ImportExportModelAdmin):
    model = UnitOfMeasurement


class CountryAdmin(ImportExportModelAdmin):
    model = Country


admin.site.register(Country)
admin.site.register(Currency)
admin.site.register(UnitOfMeasurement)
