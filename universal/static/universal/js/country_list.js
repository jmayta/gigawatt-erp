document.addEventListener("DOMContentLoaded", function() {
    $(".countries-datatable").DataTable({
        ajax: {
            url: "/api/v1/universal/countries",
            dataSrc: ""
        },
        columns: [
            {
                data: "id",
                render: function (data, type, row, meta) {
                    return `<a href="${data}/">${data}</a>`
                }
            },
            {data: "name"},
            {data: "alpha_2"},
            {data: "alpha_3"}
        ],
        columnDefs: [
            {
                targets: 0,
                width: "45px",
            },
            {
                targets: [2,3],
                width: "120px",
            }
        ],
    });
});
