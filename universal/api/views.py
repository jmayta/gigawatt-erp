from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)

from ..models import (
    Country,
    Currency,
    ExchangeRate,
    # ExpenseType,
    UnitOfMeasurement
)
from .serializers import (
    CountrySerializer,
    CurrencySerializer,
    ExchangeRateSerializer,
    # ExpenseTypeSerializer,
    UnitOfMeasurementSerializer
)


class ListCreateCountry(ListCreateAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class RetrieveUpdateDestroyCountry(RetrieveUpdateDestroyAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class ListCreateCurrency(ListCreateAPIView):
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer


class RetrieveUpdateDestroyCurrency(RetrieveUpdateDestroyAPIView):
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer


class ListCreateExchangeRate(ListCreateAPIView):
    queryset = ExchangeRate.objects.all()
    serializer_class = ExchangeRateSerializer


class RetrieveUpdateDestroyExchangeRate(RetrieveUpdateDestroyAPIView):
    queryset = ExchangeRate.objects.all()
    serializer_class = ExchangeRateSerializer


# class ListCreateExpenseType(ListCreateAPIView):
#     queryset = ExpenseType.objects.all()
#     serializer_class = ExpenseTypeSerializer


# class RetrieveUpdateDestroyExpenseType(RetrieveUpdateDestroyAPIView):
#     queryset = ExpenseType.objects.all()
#     serializer_class = ExpenseTypeSerializer


class ListCreateUnitOfMeasurement(ListCreateAPIView):
    queryset = UnitOfMeasurement.objects.all()
    serializer_class = UnitOfMeasurementSerializer


class RetrieveUpdateDestroyUnitOfMeasurement(
    RetrieveUpdateDestroyAPIView
):
    queryset = UnitOfMeasurement.objects.all()
    serializer_class = UnitOfMeasurementSerializer
