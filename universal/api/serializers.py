from rest_framework import serializers

from ..models import (
    Country,
    Currency,
    ExchangeRate,
    # ExpenseType,
    UnitOfMeasurement
)


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Country


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Currency


class ExchangeRateSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = ExchangeRate


# class ExpenseTypeSerializer(serializers.ModelSerializer):
#     class Meta:
#         fields = "__all__"
#         model = ExpenseType


class UnitOfMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = UnitOfMeasurement
