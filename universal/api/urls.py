from django.urls import path

from . import views


urlpatterns = [
    path(
        # /api/v1/universal/ countries/
        "countries/",
        views.ListCreateCountry.as_view(),
        name="country-list"
    ),
    path(
        # /api/v1/universal/ countries/5/
        "countries/<int:pk>/",
        views.RetrieveUpdateDestroyCountry.as_view(),
        name="country-detail"
    ),
    path(
        # /api/v1/universal/ currencies/
        "currencies/",
        views.ListCreateCurrency.as_view(),
        name="currency-list"
    ),
    path(
        # /api/v1/universal/ currencies/5/
        "currencies/<int:pk>/",
        views.RetrieveUpdateDestroyCurrency.as_view(),
        name="currency-detail"
    ),
    path(
        # /api/v1/universal/ xrates/
        "xrates/",
        views.ListCreateExchangeRate.as_view(),
        name="exchange-rate-list"
    ),
    path(
        # /api/v1/universal/ xrates/5/
        "xrates/<int:pk>/",
        views.RetrieveUpdateDestroyExchangeRate.as_view(),
        name="exchange-rate-detail"
    ),
    # path(
    #     # /api/v1/universal/ xtypes/
    #     "xtypes/",
    #     views.ListCreateExpenseType.as_view(),
    #     name="expense-types-list"
    # ),
    # path(
    #     # /api/v1/universal/ xtypes/5/
    #     "xtypes/<int:pk>/",
    #     views.RetrieveUpdateDestroyExpenseType.as_view(),
    #     name="expense-types-detail"
    # ),
    path(
        # /api/v1/universal/ uom/
        "uom/",
        views.ListCreateUnitOfMeasurement.as_view(),
        name="uom-list"
    ),
    path(
        # /api/v1/universal/ uom/5/
        "uom/<int:pk>/",
        views.RetrieveUpdateDestroyUnitOfMeasurement.as_view(),
        name="uom-detail"
    ),
]
