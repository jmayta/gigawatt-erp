from django.shortcuts import render

from .models import Move, MoveThing


def move_list(request):
    object_list = Move.objects.all().order_by("-code", "origin", "destination")
    context = {
        "object_list": object_list
    }
    return render(request, "moves/move_list.html", context)


def move_detail(request, move_id):
    object = Move.objects.get(pk=move_id)
    context = {
        "object": object
    }
    return render(request, "moves/move_detail.html", context)


def move_detail_print(request, move_id):
    object = Move.objects.get(pk=move_id)
    context = {
        "object": object
    }
    return render(request, "moves/move_detail_print.html", context)
