# Generated by Django 2.2.8 on 2020-02-19 05:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('things', '0001_initial'),
        ('universal', '0001_initial'),
        ('warehouses', '0001_initial'),
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Move',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, null=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(blank=True, null=True, verbose_name='updated at')),
                ('code', models.PositiveIntegerField(unique_for_year='created_at', verbose_name='code')),
                ('kind', models.CharField(choices=[('ip', 'input'), ('op', 'output')], default='op', max_length=2, verbose_name='move kind')),
                ('address_origin', models.TextField(blank=True, default='', verbose_name='origin address')),
                ('address_destination', models.TextField(blank=True, default='', verbose_name='destination address')),
                ('shipping_date', models.DateField(blank=True, null=True, verbose_name='shipping date')),
                ('document', models.FileField(blank=True, null=True, upload_to='moves/%Y/', verbose_name='despatch document')),
                ('document_reference', models.CharField(blank=True, default='', max_length=100, verbose_name='despatch document reference')),
                ('total_weight', models.FloatField(blank=True, default=0.0, verbose_name='total weight')),
                ('destination', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='receipts', to='warehouses.Warehouse', verbose_name='destination')),
                ('order', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='shippings', to='orders.Order', verbose_name='order')),
                ('origin', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='deliveries', to='warehouses.Warehouse', verbose_name='origin')),
            ],
            options={
                'verbose_name': 'move',
                'verbose_name_plural': 'moves',
            },
        ),
        migrations.CreateModel(
            name='MoveThing',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(blank=True, default='', max_length=255, verbose_name='description')),
                ('demand', models.FloatField(default=0.0, verbose_name='demand')),
                ('weight', models.FloatField(default=0.0, verbose_name='weight')),
                ('move', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='things', to='moves.Move', verbose_name='move')),
                ('serial_number', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='things.Identifiable', verbose_name='serial number')),
                ('thing', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='moves', to='things.Thing', verbose_name='thing')),
                ('uom', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='universal.UnitOfMeasurement', verbose_name='unit of measurement')),
            ],
        ),
    ]
