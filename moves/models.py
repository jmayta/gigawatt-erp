from decimal import Decimal

from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import gettext_lazy as _

from orders.models import Order
from sunqu.behaviors import Timestampable
from things.models import Thing, Identifiable
from universal.models import UnitOfMeasurement
from warehouses.models import Warehouse


class Move(Timestampable, models.Model):
    # Tipos de movimiento (opciones)
    MOVE_KIND_INPUT = "ip"
    MOVE_KIND_OUTPUT = "op"
    MOVE_KIND_CHOICES = (
        (MOVE_KIND_INPUT, _("input")),  # entrada
        (MOVE_KIND_OUTPUT, _("output"))  # salida
    )

    # Nro. referencia / Código
    code = models.PositiveIntegerField(
        _("code"),
        unique_for_year="created_at"
    )
    # Tipo de movimiento
    kind = models.CharField(
        _("move kind"),
        choices=MOVE_KIND_CHOICES,
        default=MOVE_KIND_OUTPUT,
        max_length=2
    )
    # Almacén Origen
    origin = models.ForeignKey(
        Warehouse,
        on_delete=models.PROTECT,
        related_name="deliveries",
        verbose_name=_("origin")
    )
    # Dirección Origen
    address_origin = models.TextField(
        _("origin address"),
        blank=True,
        default=""
    )
    # Almacén Destino
    destination = models.ForeignKey(
        Warehouse,
        on_delete=models.PROTECT,
        related_name="receipts",
        verbose_name=_("destination")
    )
    # Dirección Destino
    address_destination = models.TextField(
        _("destination address"),
        blank=True,
        default=""
    )
    # Inicio de traslado (fecha)
    shipping_date = models.DateField(
        _("shipping date"),
        blank=True,
        null=True
    )
    # Compra
    order = models.ForeignKey(
        Order,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="shippings",
        verbose_name=_("order")
    )
    # Documento relacionado (Guia)
    document = models.FileField(
        blank=True,
        null=True,
        upload_to="moves/%Y/",
        verbose_name=_("despatch document")
    )
    # Documento relacionado, referencia (Guia)
    document_reference = models.CharField(
        _("despatch document reference"),
        blank=True,
        default="",
        max_length=100
    )
    # Peso total
    total_weight = models.FloatField(
        _("total weight"),
        blank=True,
        default=0.00
    )

    class Meta:
        verbose_name = _("move")
        verbose_name_plural = _("moves")

    def __str__(self):
        return f"{self.code}-{self.created_at.year}"


class MoveThing(models.Model):
    # Movimiento
    move = models.ForeignKey(
        Move,
        on_delete=models.CASCADE,
        related_name="things",
        verbose_name=_("move")
    )
    # Artículo
    thing = models.ForeignKey(
        Thing,
        on_delete=models.PROTECT,
        related_name="moves",
        verbose_name=_("thing")
    )
    # Descripcion/nombre
    description = models.CharField(
        _("description"),
        blank=True,
        default="",
        max_length=255
    )
    # Nro de Serie
    serial_number = models.ForeignKey(
        Identifiable,
        blank=True,
        null=True,
        on_delete=models.PROTECT,
        verbose_name=_("serial number")
    )
    # Demanda inicial
    demand = models.FloatField(_("demand"), default=0.00)
    # Unidad de Medida
    uom = models.ForeignKey(
        UnitOfMeasurement,
        on_delete=models.PROTECT,
        verbose_name=_("unit of measurement")
    )
    # Peso (kg)
    weight = models.FloatField(_("weight"), default=0.00)

    def __str__(self):
        return f"{self.thing}"
