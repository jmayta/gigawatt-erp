import re

from sunqu.utils import codify


def new_code():
    """
    Retorna un nuevo código de movimiento
    """
    from datetime import datetime
    from .models import Move

    new_code = 1
    try:
        # -> intentar obtener el último registro creado éste año
        latest_entry = Move.objects.filter(
            created_at__year=datetime.utcnow().year
        ).latest("created_at")
    except Move.DoesNotExist:
        # -> intento fallido
        return new_code
    # -> intento satisfactorio
    # obtener código de último registro
    latest_code = latest_entry.code
    # código correlativo
    new_code = latest_code + 1
    return new_code
