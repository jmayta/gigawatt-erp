from django.urls import path

from . import views


app_name = "moves"

urlpatterns = [
    path("", views.move_list, name="list"),
    path("<int:move_id>", views.move_detail, name="detail"),
    path("<int:move_id>/print", views.move_detail_print, name="print"),
]
