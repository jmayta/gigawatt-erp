from django.contrib import admin

from .models import Move, MoveThing
# from .utils import new_code


# class MoveItemInline(admin.TabularInline):
#     model = MoveItem


# class MoveAdmin(admin.ModelAdmin):
#     exclude = (
#         "code",
#         "created_at",
#         "updated_at",
#     )
#     inlines = [
#         MoveItemInline,
#     ]

#     def save_model(self, request, obj, form, change):
#         if not change:
#             # -> si no es una modificación
#             # generar y asignar nuevo código
#             obj.code = new_code()
#         # guardar cambios
#         super().save_model(request, obj, form, change)


admin.site.register(Move)
