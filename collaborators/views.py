from django.urls import reverse
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView
)

from .models import Collaborator

from . import utils


# Lista de colaboradores
class CollaboratorList(ListView):
    model = Collaborator
    paginate_by = 15
    queryset = utils.collaborator_list()


# Detalle de colaborador
class CollaboratorDetail(DetailView):
    model = Collaborator

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pass


# Agregar nuevo colaborador
class CollaboratorCreate(CreateView):
    model = Collaborator
    fields = '__all__'


# Modificación de colaboradores
class CollaboratorUpdate(UpdateView):
    model = Collaborator
    fields = '__all__'


# TODO: Eliminación de colaborador
