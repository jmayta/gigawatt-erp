from django.contrib.auth import get_user_model

from rest_framework import serializers

# from ..models import Collaborator

# Declarando instancia global para "User"
User = get_user_model()


# class CollaboratorSerializer(serializers.ModelSerializer):

#     class Meta:
#         fields = (
#             "id",
#             "identity_document_number",
#             "fiscal_number",
#             "position",
#             "emergency_phone",
#             "emergency_contact",
#             "phone",
#             "profile_image",
#             "timezone",
#         )
#         model = Collaborator


class UserSerializer(serializers.ModelSerializer):
    # collaborator = CollaboratorSerializer(required=False)

    class Meta:
        extra_kwargs = {
            "password": {"write_only": True}
        }
        fields = "__all__"
        model = User

    def create(self, validated_data):
        user = User.objects.create(**validated_data)
        user.set_password(validated_data.get("password"))
        user.save()
        return user
