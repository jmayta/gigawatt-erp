from django.urls import path

from collaborators.api import views


urlpatterns = [
    path(
        "",
        views.UserList.as_view(),
        name="user-list"
    ),
    path(
        "<int:pk>",
        views.UserDetail.as_view(),
        name="user-detail"
    ),
]
