from django.contrib.auth import get_user_model

from rest_framework import generics

# from ..models import Collaborator
from . import serializers

# Declarando instancia del modelo "usuario"
User = get_user_model()


class UserList(generics.ListCreateAPIView):
    serializer_class = serializers.UserSerializer

    def get_queryset(self):
        queryset = User.objects.all()
        return queryset


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.UserSerializer

    def get_queryset(self):
        queryset = User.objects.all()
        return queryset
