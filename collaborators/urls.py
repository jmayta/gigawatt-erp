from django.urls import path

from . import views

app_name = 'collaborators'

urlpatterns = [
    path('', views.CollaboratorList.as_view(), name='list'),
    path('<int:pk>/', views.CollaboratorDetail.as_view(), name='detail'),
    path('register/', views.CollaboratorCreate.as_view(), name='register'),
]
