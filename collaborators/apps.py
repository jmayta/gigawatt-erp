from django.apps import AppConfig


class CollaboratorsConfig(AppConfig):
    name = 'collaborators'

    def ready(self):
        from . import signals
