from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy

from .utils import collaborator_image_path

from sunqu.behaviors import Timestampable


# Declarando instancia del modelo "usuario"
User = get_user_model()


class Collaborator(Timestampable, models.Model):
    # Número de documento de identidad
    identity_document_number = models.CharField(
        _("identity document number"),
        max_length=8,
        blank=True,
        help_text=_("Max. 8 numeric characters")
    )
    # Número fiscal (RUC)
    fiscal_number = models.CharField(
        _("fiscal number"),
        max_length=11,
        blank=True,
        default=""
    )
    # Cargo / posición
    position = models.CharField(
        _("position"), max_length=255, blank=True, default="")
    # Teléfono de emergencia
    emergency_phone = models.CharField(
        _("emergency phone"), max_length=20)
    # Contacto de emergencia
    emergency_contact = models.CharField(
        _("emergency contact"), max_length=255)
    # Teléfono
    phone = models.CharField(_("phone"), max_length=20, blank=True, default="")
    # Imagen de perfil
    profile_image = models.ImageField(
        _("profile image"),
        help_text=_("Max dimensions: 333 x 333 pixels"),
        upload_to=collaborator_image_path,
        null=True,
        blank=True
    )
    # En vacaciones?
    on_vacation = models.BooleanField(default=False)
    # Usuario vinculado
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        verbose_name=_("user")
    )
    # Timezone
    timezone = models.CharField(_("timezone"), default="UTC", max_length=150)

    def __str__(self):
        if self.display_name.strip() != "":
            return f"{self.display_name}"
        return f"{self.user}"

    def get_absolute_url(self):
        return reverse("collaborators:list")

    # Properties
    # --------------------------------------------------------------------------
    @property
    def first_name(self):
        try:
            first_name = self.user.first_name.split(" ")[0]
        except IndexError:
            first_name = self.user.get_username()
        return f"{first_name}"

    @property
    def display_name(self):
        try:
            first_name = self.user.first_name.split(" ")[0]
            last_name = self.user.last_name.split(" ")[0]
            display_name = " ".join([first_name, last_name])
        except IndexError:
            display_name = self.user.get_username()
        return f"{display_name}"
