import pdb


# Lista de colaboradores
def collaborator_list():
    from collaborators.models import Collaborator
    return Collaborator.objects.all()


# Lista de colaboradores activos
def active_collaborators():
    from collaborators.models import Collaborator
    return Collaborator.objects.filter(state='AC')


# Lista de colaboradores deshabilitados
def disabled_collaborators():
    from collaborators.models import Collaborator
    return Collaborator.objects.filter(state='IC')


# Lista de proyectos en la que el colaborador
# participa, con sus respectivos roles
def project_list():
    pass


def collaborator_image_path(instance, filename):
    extension = filename.split(".")[-1]  # ej: 'png'
    return f"profiles/{instance.user.username}.{extension}"
