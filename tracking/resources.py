from import_export.fields import Field
from import_export.resources import ModelResource

from .models import Tracking


class TrackingResource(ModelResource):
    kind = Field(attribute="get_kind_display", column_name="kind")
    payment_condition = Field(
        attribute="get_payment_condition_display",
        column_name="payment_condition"
    )

    class Meta:
        model = Tracking
        fields = (
            "id",
            "logistic_operator",
            "logistic_operator__business_name",
            "kind",
            "external_reference",
            "internal_reference",
            "contact_origin",
            "contact_destination",
            "shipping_date",
            "arrival_date",
            "payment_condition",
            "invoice",
            "total_amount",
            "content",
            "quantity",
            "project",
            "project__alias",
        )
