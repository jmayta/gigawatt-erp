from django.contrib import admin

from import_export.admin import ImportExportModelAdmin

from .models import Tracking
from .resources import TrackingResource


class TrackingAdmin(ImportExportModelAdmin):
    resource_class = TrackingResource

    list_display = [
        "id",
        "logistic_operator",
        "kind",
        "external_reference",
        "internal_reference",
        "contact_origin",
        "contact_destination",
        "shipping_date",
        "arrival_date",
        "payment_condition",
        "invoice",
        "total_amount",
        "content",
        "quantity",
        "project"
    ]

    search_fields = [
        "external_reference",
        "internal_reference",
        "contact_origin",
        "contact_destination",
    ]

    list_filter = ("kind", "logistic_operator", "payment_condition",)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "logistic_operator":
            from partners.models import Partner
            kwargs["queryset"] = Partner.objects.filter(
                is_provider=True).filter(
                    is_logistic_operator=True)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


# Register your models here.
admin.site.register(Tracking, TrackingAdmin)
