document.addEventListener('DOMContentLoaded', function() {
    $('.datatable-responsive').DataTable({
        columnDefs: [
            {
                className: 'control',
                orderable: false,
                targets: [ 0 ],
                width: 30,
                defaultContent: ""
            },
            {
                targets: [ 2 , 5 , 8 ],
                width: 350
            },
            {
                orderable: false,
                targets: [ -1 ],
                width: 50
            }
        ]
    });
});