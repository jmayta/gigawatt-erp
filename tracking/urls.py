from django.urls import path

from .views import TrackingList

app_name = "tracking"


urlpatterns = [
    path("", TrackingList.as_view(), name="list"),
]
