from django.shortcuts import render
from django.views.generic import ListView

from .models import Tracking


class TrackingList(ListView):
    model = Tracking
