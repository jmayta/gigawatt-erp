from datetime import datetime
from decimal import Decimal

from django.db import models
from django.utils import timezone

from projects.models import Project
from partners.models import Partner


# Create your models here.
class Tracking(models.Model):
    INPUT = "ip"
    OUTPUT = "op"
    TRACKING_KIND_CHOICES = (
        (INPUT, "Entrada"),
        (OUTPUT, "Salida")
    )
    CREDIT = "cr"
    PAYED = "pd"
    PAYMENT_CONDITION_CHOICES = (
        (CREDIT, "Crédito"),
        (PAYED, "Pagado")
    )
    logistic_operator = models.ForeignKey(
        Partner,
        on_delete=models.CASCADE,
        verbose_name="Operador Logístico"
    )
    kind = models.CharField(
        "Tipo",
        choices=TRACKING_KIND_CHOICES,
        max_length=2
    )
    # Referencia Externa
    external_reference = models.CharField(
        "Ref. Externa",
        blank=True, default="",
        max_length=254,
        help_text="Remito / Guia Remision Transportista (Externo)")
    # Referencia Interna
    internal_reference = models.CharField(
        "Ref. Interna",
        blank=True, default="",
        max_length=254,
        help_text="Ord. Servicio / Guia Remisión Remitente")
    invoice = models.CharField(
        "Factura", blank=True, default="", max_length=254)
    contact_origin = models.TextField(
        "Contacto Origen",
        blank=True,
        default=""
    )
    address_origin = models.TextField(
        "Dirección origen",
        blank=True,
        default=""
    )
    contact_destination = models.TextField(
        "Contacto Destino",
        blank=True,
        default=""
    )
    address_destination = models.TextField(
        "Dirección destino", blank=True, default="")
    shipping_date = models.DateField("Fecha Envío", default=timezone.now)
    arrival_date = models.DateField("Fecha Recepción", default=timezone.now)
    payment_condition = models.CharField(
        "Condición de pago",
        choices=PAYMENT_CONDITION_CHOICES,
        max_length=2
    )
    total_amount = models.DecimalField(
        "Monto Total",
        max_digits=7,
        decimal_places=3,
        default=Decimal("0.000")
    )
    content = models.TextField(
        "Contenido",
        help_text="Contenido del paquete")
    quantity = models.FloatField("Cantidad", default=1.0)
    weight = models.FloatField("Peso", default=0.0)
    volume = models.FloatField("Volumen", default=0.0)
    project = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        verbose_name="Proyecto"
    )
    observation = models.TextField("Observaciones", blank=True, default="")

    def __str__(self):
        return f"{self.logistic_operator} / {self.external_reference}"
