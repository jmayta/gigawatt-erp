
// Setup module
// ------------------------------

var DatatableResponsive = function() {

    //
    // Setup module components
    //

    // Datatable Responsive
    var _componentDatatableResponsive = function() {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }

        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            responsive: true,
            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                // url: 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json',
                aria: {
                    sortAscending: ": activar para ordenar la columna ascendentemente",
                    sortDescending: ": activar para ordenar la columna descendentemente"
                },
                // decimal: '',
                emptyTable: 'Ningún dato disponible en esta tabla',
                info: 'Mostrando del _START_ al _END_ de _TOTAL_ registros',
                infoEmpty: 'Mostrando del 0 al 0 de 0 registros',
                infoFiltered: '(filtrado de _MAX_ registros en total)',
                // infoPostFix: '',
                lengthMenu: '<span>Mostrar:</span> _MENU_',
                loadingRecords: 'Cargando...',
                paginate: {
                    'first': '&laquo;',
                    'last': '&raquo;',
                    'next': $('html').attr('dir') == 'rtl' ? '&lsaquo;' : '&rsaquo;',
                    'previous': $('html').attr('dir') == 'rtl' ? '&rsaquo;' : '&lsaquo;'
                },
                processing: 'Procesando...',
                search: '<span>Filtrar:</span> _INPUT_',
                searchPlaceholder: 'Escriba para filtrar...',
                // thousands: ',',
                zeroRecords: 'No se encontraron resultados'
            }
        });
    };

    // Select2 for length menu styling
    var _componentSelect2 = function() {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentDatatableResponsive();
            _componentSelect2();
        }
    }

}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    DatatableResponsive.init();
});
