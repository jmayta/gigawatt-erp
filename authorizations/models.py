from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from django.db import models

from sunqu.behaviors import OptionallyGenericRelateable


User = get_user_model()


class Authorization(OptionallyGenericRelateable, models.Model):
    STATE_PENDING = "pd"
    STATE_APPROVED = "ap"
    STATE_REJECTED = "rj"
    STATE_CHOICES = (
        (STATE_PENDING, _("pending")),  # pendiente
        (STATE_APPROVED, _("approved")),  # aprobado
        (STATE_REJECTED, _("rejected")),  # rechazado/anulado
    )
    authorizer = models.ForeignKey(
        User,
        null=True,
        on_delete=models.SET_NULL
    )
    state = models.CharField(
        choices=STATE_CHOICES,
        default=STATE_PENDING,
        max_length=2
    )
    observations = models.TextField(
        blank=True,
        default=""
    )

    class Meta:
        verbose_name = _("authorization")
        verbose_name_plural = _("authorizations")

    def __str__(self):
        return f"{self.content_object} / {self.authorizer} / {self.state}"
