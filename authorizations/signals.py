from django.db.models.signals import post_save
from django.dispatch import receiver


@receiver(post_save, sender="Authorization")
def notify_authorization(sender, instance, created, update_fields, **kwargs):
    """Notifica a <autorizador> o al <solicitante> la creación o el cambio de
    estado de la autorización respectivamente."""

    # Si se acaba de crear la autorización -> notificar al <autorizador>
    # obtener autorizador
    # enviar email
    pass
