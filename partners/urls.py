from django.urls import path
from . import views


app_name = "partners"

urlcustomers = []
urlproviders = []

urlpatterns = [
    # Socios de Negocio / Proveedores
    path("", views.PartnerList.as_view(), name="partner-list"),
    path("<int:pk>/", views.PartnerDetail.as_view(), name="partner-detail"),
    path("providers/", views.ProviderList.as_view(), name="provider-list"),
    path("customers/", views.CustomerList.as_view(), name="customer-list"),
    # Socios de Negocio / Clientes
    # Socios de Negocio / Proveedores / Contactos
    # path("providers/<int:pk>/", views.ProviderDetail.as_view(), name="detail"),
]
