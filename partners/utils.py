from .models import Partner


# Clientes
def customers():
    return Partner.objects.filter(
        is_customer=True,
        is_company=True
    ).order_by('business_name')


# Proveedores
def providers():
    return Partner.objects.filter(
        is_vendor=True
    ).order_by('business_name')


# Proveedores que son compañías
def company_list():
    return Partner.objects.filter(is_company=True)


# Lista de bancos
def bank_list():
    return Partner.objects.filter(is_bank=True)


# Lista de compañías activas
def active_companies():
    return Partner.objects.filter(is_active=True).filter(is_company=True)


# Lista de compañías inactivas
def inactive_companies():
    return company_list.filter(is_active=False)


# Lista de clientes
def customer_list():
    return Partner.objects.filter(is_customer=True)


# Lista de contactos de proveedor
def provider_contact_list(provider_pk):
    contacts = []
    if provider_pk:
        provider = Partner.objects.get(pk=provider_pk)
        contacts = provider.contacts.all()
    return contacts
