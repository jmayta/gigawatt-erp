from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import (
    ListView,
    DetailView,
    UpdateView,
    CreateView,
    DeleteView
)

from .models import Partner
from . import utils


class PartnerList(LoginRequiredMixin, ListView):
    model = Partner


class CustomerList(LoginRequiredMixin, ListView):
    model = Partner
    queryset = Partner.objects.filter(is_customer=True)
    template_name = "partners/customer_list.html"


class ProviderList(LoginRequiredMixin, ListView):
    model = Partner
    queryset = Partner.objects.filter(is_vendor=True)
    template_name = "partners/provider_list.html"


class PartnerDetail(DetailView):
    model = Partner

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["directory"] = self.object.directory.all()
        context["accounts"] = self.object.accounts.order_by("description")
        return context
