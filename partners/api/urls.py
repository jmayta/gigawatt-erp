from django.urls import path

from . import views


urlpatterns = [
    path("", views.ListCreatePartner.as_view(), name="partner-list"),
    path(
        "<int:pk>/",
        views.RetrieveUpdateDestroyPartner.as_view(),
        name="partner-detail"
    ),
]
