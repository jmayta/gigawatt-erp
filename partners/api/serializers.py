from rest_framework import serializers

from ..models import Partner


class PartnerSerializer(serializers.ModelSerializer):
	class Meta:
		extra_kwargs = {
			"created_at": {"read_only": True},
			"updated_at": {"read_only": True}
		}
		fields = "__all__"
		model = Partner
