from rest_framework import generics

from ..models import Partner
from .serializers import PartnerSerializer


class ListCreatePartner(generics.ListCreateAPIView):
	queryset = Partner.objects.all()
	serializer_class = PartnerSerializer


class RetrieveUpdateDestroyPartner(generics.RetrieveUpdateDestroyAPIView):
	queryset = Partner.objects.all()
	serializer_class = PartnerSerializer
