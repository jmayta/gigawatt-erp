from django.contrib import admin

from import_export.admin import ImportExportModelAdmin

from .models import Partner, Account


class AccountInline(admin.TabularInline):
    model = Account
    exclude = ("created_at", "updated_at",)


class PartnerAdmin(ImportExportModelAdmin):
    filter_horizontal = (
        "directory",
    )

    inlines = [
        AccountInline,
    ]

    list_display = [
        "fiscal_number",
        "business_name",
        "email",
        "is_active",
    ]

    search_fields = [
        "business_name",
        "fiscal_number",
    ]


admin.site.register(Partner, PartnerAdmin)
