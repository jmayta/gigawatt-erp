from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.postgres import fields
from django.db import models
from django.utils.translation import gettext_lazy as _

from contacts.models import Contact
from universal.models import Country
from sunqu.behaviors import Timestampable


class Partner(Timestampable, models.Model):
    """ Socios de Negocio """
    # RUC / DNI
    fiscal_number = models.CharField(_("fiscal number"), max_length=25)
    # Razón social anterior
    old_business_name = models.CharField(
        _("old bussines name"),
        blank=True,
        default="",
        max_length=255
    )
    # Razón social
    business_name = models.CharField(
        _("business name"),
        max_length=255
    )
    # Nombre comercial
    tradename = models.CharField(
        _("tradename"),
        blank=True,
        default="",
        max_length=255
    )
    # Alias
    alias = models.CharField(
        _("alias"),
        blank=True,
        default="",
        max_length=150
    )
    # Alias corto
    short_alias = models.CharField(
        _("short alias"),
        blank=True,
        default="",
        max_length=20
    )
    # Dirección Fiscal
    fiscal_address = models.TextField(
        _("fiscal address"),
        blank=True,
        default=""
    )
    # Dirección segunda línea
    address_second_line = models.CharField(
        _("address second line"),
        blank=True,
        default="",
        max_length=254
    )
    # Localización
    location = models.CharField(
        _("location"),
        blank=True,
        default="",
        help_text=_("District, City, State"),
        max_length=254
    )
    # Código de Ubicación Geográfica
    # https://www.reniec.gob.pe/Adherentes/jsp/ListaUbigeos.jsp
    ubigeo = models.CharField(
        _("ubigeo code"),
        blank=True,
        default="",
        help_text="https://www.reniec.gob.pe/Adherentes/jsp/ListaUbigeos.jsp",
        max_length=6
    )
    # País
    country = models.ForeignKey(
        Country, on_delete=models.PROTECT, verbose_name=_("country"))
    # ¿Está activo?
    is_active = models.BooleanField(_("is active"), default=True)
    # ¿Es una compañía?
    is_company = models.BooleanField(
        _("is company"),
        default=True,
        help_text=_("mark if it's a company, blank if is a person.")
    )
    # ¿Es un cliente?
    is_customer = models.BooleanField(_("is customer"))
    # ¿Es un proveedor?
    is_vendor = models.BooleanField(_("is provider"))
    # ¿Es agente de retencion?
    is_retention_agent = models.BooleanField(
        _("is_retention_agent"),
        default=False
    )
    # Categorías / Rubros
    categories = fields.ArrayField(
        models.CharField(blank=True, max_length=150),
        blank=True,
        null=True,
        verbose_name=_("categories")
    )
    # Email
    email = models.EmailField(_("email"), blank=True, default="")
    # Teléfonos
    phones = fields.ArrayField(
        models.CharField(max_length=150, blank=True, default=""),
        blank=True,
        null=True,
        verbose_name=_("phones")
    )
    # Sitio web
    web_site = models.TextField(_("web site"), blank=True, default="")
    # Fecha de baja
    deactivation_date = models.DateTimeField(
        _("deactivation date"),
        blank=True,
        null=True
    )
    # Contactos
    directory = models.ManyToManyField(
        Contact,
        blank=True,
        related_name="partners"
    )
    # Bancos
    banks = models.ManyToManyField(
        "self",
        symmetrical=False,
        through="Account"
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["fiscal_number"], name="unique_fiscal_number"
            ),
            models.UniqueConstraint(
                fields=["business_name"], name="unique_business_name"
            )
        ]
        ordering = ["business_name"]

    def __str__(self):
        return f"{self.business_name}"


class Account(Timestampable, models.Model):
    """ Cuentas bancarias """
    # socio de negocio
    partner = models.ForeignKey(
        Partner,
        on_delete=models.CASCADE,
        related_name="accounts",
        verbose_name=_("partner")
    )
    # descripción / nombre
    description = models.CharField(
        _("description"),
        max_length=255
    )
    # número de cuenta
    number = models.CharField(
        _("account number"),
        max_length=50
    )
    # número de cuenta interbancario
    interbank_code = models.CharField(
        _("interbank code"),
        blank=True,
        help_text=_("CCI"),
        max_length=50,
        null=True
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["number"], name="unique_account_number"
            ),
            models.UniqueConstraint(
                fields=["interbank_code"], name="unique_interbank_code"
            )
        ]

    def __str__(self):
        return f"{self.number}"
