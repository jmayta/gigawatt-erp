document.addEventListener('DOMContentLoaded', function() {
    $('.partner-datatable').DataTable({
        responsive: {
            details: {
                type: "column"
            }
        },
        columnDefs: [
            {
                className: 'control',
                orderable: false,
                targets: 0,
                width: 30,
                defaultContent: ""
            },
            {
                targets: 1,
                width: "180px"
            },
            {
                targets: [3, 4, 5],
                width: "130px"
            },
            {
                orderable: false,
                targets: [ -1 ],
                width: "30px"
            }
        ],
        // order: [1, 'desc']
    });


    $('.customer-datatable').DataTable({
        responsive: {
            details: {
                type: "column"
            }
        },
        columnDefs: [
            {
                className: 'control',
                orderable: false,
                targets: 0,
                width: 30,
                defaultContent: ""
            },
            {
                targets: 1,
                width: "180px"
            },
            {
                targets: 3,
                width: "50px"
            },
            {
                orderable: false,
                targets: [ -1 ],
                width: "30px"
            }
        ],
        // order: [1, 'desc']
    });


    $('.provider-datatable').DataTable({
        responsive: {
            details: {
                type: "column"
            }
        },
        columnDefs: [
            {
                className: 'control',
                orderable: false,
                targets: 0,
                width: 30,
                defaultContent: ""
            },
            {
                targets: 1,
                width: "180px"
            },
            {
                targets: 3,
                width: "50px"
            },
            {
                orderable: false,
                targets: [ -1 ],
                width: "30px"
            }
        ],
        // order: [1, 'desc']
    });
});
