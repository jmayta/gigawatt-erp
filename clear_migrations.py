def clear_migrations():
    from pathlib import Path, PurePath
    from django.conf import settings
    path = Path(settings.BASE_DIR)
    [print(x.name) for x in path.rglob("migrations/*.py") if x.name != "__init__.py"]
    answer = input("¿Seguro que quiere borrar todas las migraciones? [y/N]: ")
    if answer == "y" or answer == "Y":
        for file in path.rglob("migrations/*.py"):
            if PurePath(file).name != "__init__.py":
                file.unlink()
    else:
        return

if __name__ == "__main__":
    clear_migrations()