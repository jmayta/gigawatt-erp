from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class RequisitionsConfig(AppConfig):
    name = 'requisitions'
    verbose_name = _("requisitions")

    def ready(self):
        from requisitions import signals
