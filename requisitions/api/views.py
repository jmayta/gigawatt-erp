from rest_framework import generics
from rest_framework import permissions

from requisitions.models import Requisition
from requisitions.api.serializers import (
    RequisitionListSerializer,
    RequisitionDetailSerializer
)


class RequisitionListCreateAPIView(generics.ListCreateAPIView):

    queryset = Requisition.objects.all()
    serializer_class = RequisitionListSerializer
    # permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        print(self.request.data)
        applicant = self.request.user
        serializer.save(applicant=applicant)
