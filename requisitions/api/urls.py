from django.urls import path

from requisitions.api.views import RequisitionListCreateAPIView


urlpatterns = [
    path(
        "",
        RequisitionListCreateAPIView.as_view(),
        name="requisition-list"
    )
]
