from rest_framework import serializers

from projects.models import Project
from requisitions.models import (
    Requisition,
    RequisitionThing,
)
from sunqu.utils import codify


class RequisitionThingSerializer(serializers.ModelSerializer):

    class Meta:
        model = RequisitionThing
        exclude = (
            "requisition",
            "reserved",
            "done",
        )


class RequisitionListSerializer(serializers.ModelSerializer):

    class Meta:
        fields = (
            "code",
            "scheduled_date",
            "project",
            "applicant",
            "priority",
            "created_at",
            "state",
        )
        model = Requisition


class RequisitionDetailSerializer(serializers.ModelSerializer):
    code = serializers.SerializerMethodField()
    project = serializers.StringRelatedField()
    applicant = serializers.SerializerMethodField()
    priority = serializers.SerializerMethodField()
    state = serializers.SerializerMethodField()
    updated_at = serializers.SerializerMethodField()
    created_at = serializers.SerializerMethodField()
    items = RequisitionThingSerializer(many=True, required=False)

    class Meta:
        fields = (
            "id",
            "code",
            "project",
            "applicant",
            "priority",
            "requested_date",
            "scheduled_date",
            "attention_date",
            "state",
            "updated_at",
            "created_at",
            "items",
        )
        model = Requisition

    def get_code(self, obj):
        code_formated = codify(obj.code, length=5)
        return f"RQ{code_formated}-{obj.created_at.year}"

    def get_applicant(self, obj):
        display_name = obj.applicant.collaborator.display_name
        return f"{display_name}"

    def get_priority(self, obj):
        priority_string = str.capitalize(obj.get_priority_display())
        return f"{priority_string}"

    def get_state(self, obj):
        state_string = str.capitalize(obj.get_state_display())
        return f"{state_string}"

    def get_updated_at(self, obj):
        date_formated = obj.updated_at.strftime("%Y-%m-%d %H:%M")
        return f"{date_formated}"

    def get_created_at(self, obj):
        return obj.created_at.strftime("%Y-%m-%d %H:%M")
