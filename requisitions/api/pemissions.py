from rest_framework import permissions


class IsApplicant(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        # Si el verbo enviado es de lectura
        if request.method in permissions.SAFE_METHODS:
            return True
        # Si el verbo es de escritura, verificar que sea el solicitante
        return obj.applicant == request.user
