import uuid as uuid_lib

from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from authorizations.models import Authorization
from projects.models import Project
from sunqu.behaviors import Timestampable
from sunqu.utils import codify
from things.models import Thing
from universal.models import Currency, UnitOfMeasurement

User = get_user_model()


class Requisition(Timestampable):
    # Prioridad
    PRIORITY_URGENT = "ug"  # urgente
    PRIORITY_HIGH = "hi"  # alta
    PRIORITY_MEDIUM = "md"  # media
    PRIORITY_LOW = "lw"  # baja
    PRIORITY_CHOICES = (
        (PRIORITY_URGENT, _("urgent")),
        (PRIORITY_HIGH, _("high")),
        (PRIORITY_MEDIUM, _("medium")),
        (PRIORITY_LOW, _("low")),
    )

    # Estado
    STATE_CREATED = "cr"  # creado
    STATE_OPENED = "op"  # abierto
    STATE_CLOSED = "cl"  # cerrado
    STATE_CANCELED = "cn"  # cancelado
    STATE_CHOICES = (
        (STATE_CREATED, _("created")),
        (STATE_OPENED, _("opened")),
        (STATE_CLOSED, _("closed")),
        (STATE_CANCELED, _("canceled")),
    )
    # uuid
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False
    )
    # Código de requerimiento
    code = models.PositiveIntegerField(
        _("code"),
        blank=True,
        unique_for_year="created_at"
    )
    # Proyecto
    project = models.ForeignKey(
        Project,
        on_delete=models.PROTECT,
        related_name="requisitions",
        verbose_name=_("project")
    )
    # Solicitante
    applicant = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="requisitions",
        verbose_name=_("applicant")
    )
    # Prioridad
    priority = models.CharField(
        _("priority"),
        choices=PRIORITY_CHOICES,
        default=PRIORITY_MEDIUM,
        max_length=2
    )
    # Requerido para <fecha>
    requested_date = models.DateField(
        _("requested date"),
        blank=True,
        null=True
    )
    # Programado para <fecha>
    scheduled_date = models.DateField(
        _("scheduled date"),
        blank=True,
        null=True
    )
    # Atendido el <fecha>
    attention_date = models.DateTimeField(
        _("attention date"),
        blank=True,
        null=True
    )
    # Estado de atención
    state = models.CharField(
        _("state"),
        choices=STATE_CHOICES,
        default=STATE_CREATED,
        max_length=2
    )
    # Razón de anulación
    cancel_reason = models.TextField(
        _("cancel reason"),
        blank=True,
        default=""
    )
    # Autorizaciones
    authorizations = GenericRelation(Authorization)

    class Meta:
        verbose_name = _("requisition")
        verbose_name_plural = _("requisitions")

    def __str__(self):
        str_code = codify(self.code, 5)
        str_year = self.created_at.strftime("%y")
        return f"{str_code}-{str_year}"

    def get_absolute_url(self):
        return reverse("requisitions:detail", kwargs={"pk": self.pk})


class RequisitionThing(models.Model):
    # Requerimiento
    requisition = models.ForeignKey(
        Requisition,
        on_delete=models.CASCADE,
        related_name="things"
    )
    # Artículo
    thing = models.ForeignKey(
        Thing,
        on_delete=models.PROTECT,
        related_name="requisitions",
        verbose_name=_("thing")
    )
    # Demanda / Cantidad
    demand = models.FloatField(_("demand"), default=0.00)
    # Unidad de medida
    uom = models.ForeignKey(
        UnitOfMeasurement,
        on_delete=models.PROTECT,
        verbose_name=_("unit of measurement")
    )
    # Demanda base
    # <Demanda> convertida a la unidad de medida base del artículo.
    # Ex 1: 1.5 Km -> 1500 m
    # Ex 2: 5 doc -> 60 und
    base_demand = models.FloatField(_("base demand"), default=0.00)
    # En reserva (cantidad)
    reserved = models.FloatField(_("reserved"), default=0.00)
    # Atendido (cantidad)
    done = models.FloatField(_("done"), default=0.00)
    # ¿Es permanente?
    is_permanent = models.BooleanField(
        _("permanent"),
        default=True
    )
    # Requerido desde...
    requested_from = models.DateField(
        _("requested from"), blank=True, null=True)
    # Requerido hasta...
    requested_until = models.DateField(
        _("requested until"), blank=True, null=True)

    class Meta:
        verbose_name = _("requisition detail")
        verbose_name_plural = _("requisition details")

    def __str__(self):
        return f"{self.thing}"

    def save(self, *args, **kwargs):
        self.base_demand = self.demand * self.uom.numerical_value
        super().save(*args, **kwargs)


# class Cash(Timestampable, models.Model):
#     DELIVERY_REFERENCE_CASH = "ca"
#     DELIVERY_REFERENCE_CHECK = "ch"
#     DELIVERY_REFERENCE_DEPOSIT = "dp"
#     DELIVERY_REFERENCE_TRANSFER = "tr"
#     DELIVERY_REFERENCE_CHOICES = (
#         (DELIVERY_REFERENCE_CASH, _("cash")),  # efectivo
#         (DELIVERY_REFERENCE_CHECK, _("check")),  # cheque
#         (DELIVERY_REFERENCE_DEPOSIT, _("deposit")),  # depósito
#         (DELIVERY_REFERENCE_TRANSFER, _("transfer")),  # transferencia
#     )
#     # Código de requerimiento de efectivo
#     code = models.PositiveIntegerField(
#         _("code"),
#         unique_for_year="created_at"
#     )
#     # Solicitante
#     applicant = models.ForeignKey(
#         User,
#         on_delete=models.CASCADE,
#         related_name="cash_requisitions",
#         verbose_name=_("applicant")
#     )
#     # Proyecto
#     project = models.ForeignKey(
#         Project,
#         on_delete=models.CASCADE,
#         related_name="cash_requests",
#         verbose_name=_("project")
#     )
#     # Moneda
#     currency = models.ForeignKey(
#         Currency,
#         on_delete=models.PROTECT,
#         verbose_name=_("currency")
#     )
#     # Referencia de entrega
#     delivery_reference = models.CharField(
#         _("delivery reference"),
#         choices=DELIVERY_REFERENCE_CHOICES,
#         default=DELIVERY_REFERENCE_CASH,
#         max_length=2
#     )
#     # Observaciones
#     observation = models.TextField(
#         _("observation"),
#         blank=True,
#         default="",
#         help_text=_("Observation, sustentation")
#     )
#     # Monto total
#     amount_total = models.DecimalField(
#         _("total amount"),
#         decimal_places=3,
#         default=Decimal("0.000"),
#         max_digits=13
#     )
#     # URL Comprobante de desembolso
#     disbursement_voucher = models.URLField(
#         _("disbursement voucher"), blank=True, default="")
#     # Estado de atención
#     # state = models.CharField(
#          _("state"), choices=STATE_CHOICES, max_length=2)
#     # Autorizaciones
#     authorizations = GenericRelation(Authorization)
#
#     class Meta:
#         verbose_name = _("cash requisition")
#         verbose_name_plural = _("cash requisitions")
#
#     def __str__(self):
#         from sunqu.utils import codify
#         code_string = codify(self.code, 5)
#         return f"{code_string}-{self.created_at.year}"
