from functools import reduce
from operator import or_

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import F, Q, Sum
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .models import Requisition, RequisitionThing
from projects.models import Project
# from purchases.models import Purchase
from things.models import Thing


# Registrar requerimientos
class RequisitionRegister(CreateView):
    model = Requisition


# Lista de requerimientos
class RequisitionList(ListView):
    model = Requisition

    def get_queryset(self):
        queryset = Requisition.objects.select_related(
            "applicant",
            "applicant__profile",
            "project"
        )
        return queryset


class RequisitionThingList(ListView):
    template_name = "requisitions/requisition_item_list.html"

    def get_queryset(self):
        self.queryset = RequisitionThing.objects.annotate(
            pending=F("demand")-F("done")
        ).exclude(
            pending=0
        ).select_related(
            "requisition",
            "requisition__project",
            "thing",
            "thing__default_uom",
        ).order_by("thing")
        return super().get_queryset()


# Detalle de requerimiento
class RequisitionDetail(DetailView):
    model = Requisition

    def get_queryset(self):
        new_queryset = Requisition.objects.select_related("project")
        return new_queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["items"] = self.object.things.all()
        context["purchase_list"] = self.object.orders.all()
        return context


class RequestedItemList(ListView):
    template_name = "requisitions/requested_item_list.html"

    def get_queryset(self):
        self.queryset = RequisitionThing.objects.values(
            "thing",
            "thing__description",
            "thing__default_uom__abbreviation"
        ).annotate(
            qty_pending=Sum(F("base_demand")-F("done"))
        ).filter(qty_pending__gt=0).order_by("thing__description")
        return self.queryset


# Lista de Compras relacionadas al Requerimiento
class RequisitionPurchaseList(ListView):
    template_name = "requisitions/requisition_purchase_list.html"

    def get_queryset(self):
        self.requisition = get_object_or_404(
            Requisition, pk=self.kwargs.get("pk"))
        self.queryset = Purchase.objects.filter(requisition=self.requisition)
        return super().get_queryset()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["object"] = self.requisition
        return context


# -----------------------------------------------------------------------------
# Things:Required
# -----------------------------------------------------------------------------
class ThingRequiredList(ListView):
    """Vista para lista de artículos requeridos"""
    template_name = "requisitions/requisition_thing.html"

    def get_queryset(self):
        thing = get_object_or_404(Thing, pk=self.kwargs.get("pk"))
        self.queryset = thing.required.select_related("requisition")
        return self.queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["thing"] = get_object_or_404(
            Thing, pk=self.kwargs.get("pk")
        )
        context["requisition_detail_list"] = self.get_queryset()
        return context


def thing_requisitions_list(request, thing_pk):
    thing = Thing.objects.get(pk=thing_pk)
    requisition_detail_list = thing.requisitiondetail_set.select_related(
        "requisition"
    )
    context = {
        "thing": thing,
        "requisition_detail_list": requisition_detail_list
    }
    return render(request, "requisitions/requisition_thing.html", context)


class ProjectRequisitionConcatenated(ListView):
    template_name = "requisitions/project_requisition_concatenated.html"

    def get_queryset(self):
        qs = self.request.GET.get("q", None)
        if qs is None or qs == "":
            return
        qs_arr = qs.split(" ")
        print(qs_arr)
        self.queryset = RequisitionThing.objects.filter(
            reduce(
                or_,
                (Q(requisition__project__alias__iexact=alias) for
                    alias in qs_arr)
            )
        )
        print(self.queryset)
        return super().get_queryset()
