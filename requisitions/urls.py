from django.urls import include, path

from .views import (
    ProjectRequisitionConcatenated,
    RequisitionRegister,
    RequisitionList,
    RequisitionDetail,
    RequisitionThingList,
    RequestedItemList,
    RequisitionPurchaseList,
    ThingRequiredList,
    thing_requisitions_list,
)

app_name = "requisitions"

purchases_patterns = []

things_patterns = [
    path("", RequestedItemList.as_view(), name="requested"),
    path("<int:pk>", ThingRequiredList.as_view(), name="detail")
]

urlpatterns = [
    path("add/", RequisitionRegister.as_view(), name="add"),
    path("", RequisitionList.as_view(), name="list"),
    path("<int:pk>/", RequisitionDetail.as_view(), name="detail"),
    path("lines/", RequisitionThingList.as_view(), name="lines"),
    path(
        "<int:pk>/purchases/",
        RequisitionPurchaseList.as_view(),
        name="purchases"
    ),
    # path("master/", RequestedItemList.as_view(), name="requested-item-list"),
    # Things / Artículos
    # -------------------------------------------------------------------------
    path(
        "things/",
        include(
            (things_patterns, "things"),
            namespace="things"
        )
    ),
    path("test/", ProjectRequisitionConcatenated.as_view(), name="test")
]
