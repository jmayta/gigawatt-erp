# Generated by Django 2.2.8 on 2020-02-19 05:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('things', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('universal', '0001_initial'),
        ('projects', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Requisition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, null=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(blank=True, null=True, verbose_name='updated at')),
                ('uuid', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False)),
                ('code', models.PositiveIntegerField(blank=True, unique_for_year='created_at', verbose_name='code')),
                ('priority', models.CharField(choices=[('ug', 'urgent'), ('hi', 'high'), ('md', 'medium'), ('lw', 'low')], default='md', max_length=2, verbose_name='priority')),
                ('requested_date', models.DateField(blank=True, null=True, verbose_name='requested date')),
                ('scheduled_date', models.DateField(blank=True, null=True, verbose_name='scheduled date')),
                ('attention_date', models.DateTimeField(blank=True, null=True, verbose_name='attention date')),
                ('state', models.CharField(choices=[('cr', 'created'), ('op', 'opened'), ('cl', 'closed'), ('cn', 'canceled')], default='cr', max_length=2, verbose_name='state')),
                ('cancel_reason', models.TextField(blank=True, default='', verbose_name='cancel reason')),
                ('applicant', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='requisitions', to=settings.AUTH_USER_MODEL, verbose_name='applicant')),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='requisitions', to='projects.Project', verbose_name='project')),
            ],
            options={
                'verbose_name': 'requisition',
                'verbose_name_plural': 'requisitions',
            },
        ),
        migrations.CreateModel(
            name='RequisitionThing',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('demand', models.FloatField(default=0.0, verbose_name='demand')),
                ('base_demand', models.FloatField(default=0.0, verbose_name='base demand')),
                ('reserved', models.FloatField(default=0.0, verbose_name='reserved')),
                ('done', models.FloatField(default=0.0, verbose_name='done')),
                ('is_permanent', models.BooleanField(default=True, verbose_name='permanent')),
                ('requested_from', models.DateField(blank=True, null=True, verbose_name='requested from')),
                ('requested_until', models.DateField(blank=True, null=True, verbose_name='requested until')),
                ('requisition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='things', to='requisitions.Requisition')),
                ('thing', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='requisitions', to='things.Thing', verbose_name='thing')),
                ('uom', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='universal.UnitOfMeasurement', verbose_name='unit of measurement')),
            ],
            options={
                'verbose_name': 'requisition detail',
                'verbose_name_plural': 'requisition details',
            },
        ),
    ]
