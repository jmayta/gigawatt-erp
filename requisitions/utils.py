from django.utils import timezone
from requisitions.models import Requisition, RequisitionLine


def new_requisition_code():
    ''' Retorna el código de requerimiento consecutivo como Integer. '''
    from .models import Requisition
    from sunqu import utils

    last_code = 0
    total_entries = Requisition.objects.filter(
        created_at__year=timezone.now().year
    ).count()
    if total_entries > 0:
        # obtener el ultimo registro basado a la fecha de creación
        last_entry = utils.get_last_entry(Requisition, "created_at")
        # obtener el código del último registro
        last_code = last_entry.code
    # crear el nuevo código consecutivo
    new_code = int(last_code) + 1
    return new_code


def timestamped_code():
    from datetime import datetime
    from requisitions.models import Requisition
    from sunqu.utils import codify

    timestamp_str = datetime.utcnow().strftime("%Y%m%d%H%M%S")
    # Buscar registros con códigos que inicien con
    similar_count = Requisition.objects.filter(
        code__startswith=timestamp_str
    ).count()
    if similar_count == 0:
        return timestamp_str + codify(1, 3)
    else:
        return timestamp_str + codify(similar_count+1, 3)


def is_authorized(requisition_pk: int) -> boolean:
    # Si almenos cuenta con "una" autorización por parte de los responsables
    # el estado del requerimiento será actualizado a "aprobado" y la función
    # devolverá True, sino la función deberá retornar False
    requisition = Requisition.objects.get(requisition_pk)
    chiefs = requisition.membership.all()
    return False
