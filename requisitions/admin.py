from django.contrib import admin
from django.shortcuts import redirect

from import_export.admin import ImportExportMixin, ImportExportModelAdmin

from .forms import RequisitionForm
from .models import (
    Requisition,
    RequisitionThing,
)
from sunqu.utils import generate_integer_code


class RequisitionItemsInLine(ImportExportMixin, admin.TabularInline):
    model = RequisitionThing

    fields = (
        "thing",
        "demand",
        "uom",
        "is_permanent",
        "requested_from",
        "requested_until",
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "uom":
            from universal.models import UnitOfMeasurement
            kwargs["queryset"] = UnitOfMeasurement.objects.order_by("name")
        if db_field.name == "thing":
            from things.models import Thing
            kwargs["queryset"] = Thing.objects.order_by("description")
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class RequisitionAdmin(ImportExportModelAdmin):
    form = RequisitionForm

    inlines = [
        RequisitionItemsInLine,
    ]

    list_display = [
        "code",
        "applicant",
        "project",
        "priority",
        "created_at",
        "scheduled_date",
    ]

    readonly_fields = (
        "created_at",
        "updated_at",
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # Si el nombre del campo es...
        if db_field.name == "project":
            from projects.models import Project
            # Sobrescribir el queryset
            # Mostrar sólo proyectos activos
            kwargs["queryset"] = Project.objects.filter(
                is_active=True
            ).order_by("alias")
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def response_add(self, request, obj, post_url_continue=None):
        return redirect("requisitions:list")

    def response_change(self, request, obj):
        return redirect("requisitions:detail", obj.id)

    def save_model(self, request, obj, form, change):
        # si no es una modificación
        if not change:
            # generar nuevo código
            new_code = generate_integer_code(Requisition, "code", "created_at")
            # asignar nuevo código
            obj.code = new_code
            # solicitante = usuario actual
            obj.applicant = request.user
        super().save_model(request, obj, form, change)


class RequisitionItemsAdmin(ImportExportModelAdmin):
    pass


admin.site.register(Requisition, RequisitionAdmin)
