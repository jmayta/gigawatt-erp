from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Requisition


# Se ejecutará después que el requerimiento sea guardado
@receiver(post_save, sender=Requisition)
def notify_stakeholders(sender, instance, created, **kwargs):
    """Notificar a los interesados"""
    # Notificar a solicitante
    if instance.applicant.email:
        requisition = instance
        requisition_project = instance.project
        project_period = requisition_project.award_date.year
        subject = " // ".join([
            "SYS",
            f"Requerimiento REQ-{requisition}",
            f"CP/GW-{requisition_project}-{project_period}"
        ])
        if created:
            # enviar correo de confirmación a solicitante
            message = f"Requerimiento REQ-{instance} registrado con éxito."
        else:
            message = f"Requerimiento REQ-{instance} modificado con éxito."
        instance.applicant.email_user(
            subject=subject,
            message=message,
            from_email=None
        )

    # TODO: Generar submódulo o vincular a "groups" para enviar las
    # notificaciones a los demás contactos.

    # Collaborators -----------------------------------------------------------
    # -> obtener lista de correo de usuarios/colaboradores del almacén
    # y del área de logística.

    # Authorizations ----------------------------------------------------------
    # -> Si la aplicación <authorizations> está instalada
    # Lanzar señal para crear la autorización de este requerimiento
    # Obtener lista de <autorizadores>
    # Notificar via correo a los <autorizadores>
