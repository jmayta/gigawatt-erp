document.addEventListener('DOMContentLoaded', function() {
    $('.datatable-responsive').DataTable({
        responsive: {
            details: {
                type: 'column'
            }
        },
        columnDefs: [
            {
                className: 'control',
                orderable: false,
                targets: 0,
                width: 30
            },
            {
                orderable: false,
                targets: [ 4,5 ]
            },
            {
                orderable: false,
                targets: [ -1 ],
                width: 100
            }
        ],
        order: [1, 'asc']
    });
});
