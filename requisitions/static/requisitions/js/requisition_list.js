document.addEventListener('DOMContentLoaded', function() {
    $('.datatable-responsive').DataTable({
        responsive: {
            details: {
                type: "column"
            }
        },
        columnDefs: [
            {
                className: 'control',
                orderable: false,
                targets: 0,
                width: 30,
                defaultContent: ""
            },
            {
                // code
                targets: 1,
                width: "110px"
            },
            {
                targets: [ 5, 7 ],
                width: "120px"
            },
            { 
                targets: [ 2, 3 ],
                width: "150px"
            },
            {
                targets: 6,
                width: "200px"
            }
        ],
        order: [1, 'desc']
    });
});
