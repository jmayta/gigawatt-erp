from django import forms
from django.utils.translation import gettext as _

from .models import Requisition


class RequisitionForm(forms.ModelForm):
    code = forms.IntegerField(
        disabled=True,
        label=_("Code"),
        required=False,
    )

    class Meta:
        model = Requisition
        fields = (
            "code",
            "project",
            "priority",
            "requested_date",
            "scheduled_date",
            "state",
            "attention_date",
        )
