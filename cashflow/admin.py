from django.contrib import admin

from .models import Flow, FlowThing

class FlowThingInLine(admin.TabularInline):
    model = FlowThing

class FlowAdmin(admin.ModelAdmin):
    model = Flow
    inlines = [
        FlowThingInLine,
    ]

admin.site.register(Flow, FlowAdmin)
