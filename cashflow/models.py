from decimal import Decimal
import uuid as uuid_lib

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

from partners.models import Partner
from projects.models import Project
from sunat.models import Tax
from sunqu.behaviors import Timestampable
from things.models import Thing
from universal.models import Currency, UnitOfMeasurement

User = get_user_model()


class Flow(Timestampable, models.Model):
    """Flujo de caja"""
    # Tipo de flujo (opciones)
    FLOW_INPUT = "in"  # flujo de entrada
    FLOW_OUTPUT = "ou"  # flujo de salida
    FLOW_TYPE_CHOICE = (
        (FLOW_INPUT, _("Input")),
        (FLOW_OUTPUT, _("Output")),
    )
    # Usuario / autor
    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        verbose_name=_("user")
    )
    document = models.FileField(upload_to="flow/notes/%Y/")
    # Requerimiento de efectivo
    # TODO: Implementar <requerimiento de efectivo>
    # Tipo de flujo
    flow_type = models.CharField(
        _("flow type"),
        choices=FLOW_TYPE_CHOICE,
        max_length=2
    )
    # Fecha de operación
    operation_date = models.DateField(_("operation date"))
    # Moneda
    currency = models.ForeignKey(
        Currency,
        on_delete=models.PROTECT,
        verbose_name=_("currency")
    )
    # Descripción del gasto
    description = models.TextField(
        _("description"),
        blank=True,
        default=""
    )
    # Monto total
    amount_payable = models.DecimalField(
        _("total amount"),
        decimal_places=2,
        default=Decimal(0.00),
        max_digits=13
    )
    # Cliente/Proveedor (depende del <tipo de flujo>)
    partner = models.ForeignKey(
        Partner,
        null=True,
        on_delete=models.SET_NULL,
        related_name="cash_flow",
        verbose_name=_("provider")
    )
    # PERSONA NATURAL
    # Nombre completo
    person_full_name = models.CharField(
        _("person full name"),
        blank=True,
        default="",
        max_length=255
    )
    # DNI
    person_id_number = models.CharField(
        _("person id number"),
        blank=True,
        default="",
        max_length=25
    )
    # Proyecto
    project = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        related_name="cash_flow",
        verbose_name=_("project")
    )

    def __str__(self):
        return f"{self.id}"


class FlowThing(models.Model):
    """Items/detalle del <flujo>"""
    flow = models.ForeignKey(Flow, on_delete=models.CASCADE)
    quantity = models.FloatField(_("quantity"), default=0.00)
    uom = models.ForeignKey(
        UnitOfMeasurement,
        on_delete=models.PROTECT,
        verbose_name=_("unit of measurement")
    )
    thing = models.ForeignKey(
        Thing,
        on_delete=models.PROTECT,
        verbose_name=_("thing")
    )
    description = models.CharField(
        _("description"),
        blank=True,
        default="",
        max_length=255
    )
    untaxed_unit_price = models.DecimalField(
        _("untaxed unit price"),
        decimal_places=2,
        default=Decimal(0.00),
        max_digits=13
    )
    taxes = models.ManyToManyField(Tax)
    taxed_unit_price = models.DecimalField(
        _("taxed unit price"),
        decimal_places=2,
        default=Decimal(0.00),
        max_digits=13
    )

    def __str__(self):
        return f"{self.thing}"
