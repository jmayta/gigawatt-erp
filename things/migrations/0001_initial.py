# Generated by Django 2.2.8 on 2020-02-19 05:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('universal', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Composition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.FloatField(blank=True, default=1.0, verbose_name='quantity')),
            ],
        ),
        migrations.CreateModel(
            name='Thing',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, null=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(blank=True, null=True, verbose_name='updated at')),
                ('code', models.CharField(blank=True, default='', max_length=16, verbose_name='code')),
                ('unspsc', models.CharField(blank=True, default='', max_length=8)),
                ('description', models.CharField(max_length=255, verbose_name='description')),
                ('kind', models.CharField(choices=[('st', 'Storable'), ('cs', 'Consumable'), ('sv', 'Service')], default='st', max_length=2, verbose_name='kind')),
                ('image', models.ImageField(blank=True, null=True, upload_to='things/images/', verbose_name='image')),
                ('weight', models.FloatField(default=0.0, verbose_name='weight')),
                ('components', models.ManyToManyField(through='things.Composition', to='things.Thing')),
                ('default_uom', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='universal.UnitOfMeasurement', verbose_name='unit of measurement')),
                ('parent', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='instances', to='things.Thing', verbose_name='parent')),
            ],
            options={
                'verbose_name': 'thing',
                'verbose_name_plural': 'things',
                'ordering': ['description'],
            },
        ),
        migrations.CreateModel(
            name='Identifiable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('serial_number', models.CharField(max_length=100, verbose_name='serial number')),
                ('lot', models.CharField(blank=True, default='', max_length=50, verbose_name='lot')),
                ('is_owned', models.BooleanField(default=True, verbose_name='is owned')),
                ('thing', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='identifications', to='things.Thing', verbose_name='thing')),
            ],
            options={
                'verbose_name': 'identifiable',
                'verbose_name_plural': 'identifiables',
            },
        ),
        migrations.AddField(
            model_name='composition',
            name='component',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='parents', to='things.Thing', verbose_name='component'),
        ),
        migrations.AddField(
            model_name='composition',
            name='parent',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='children', to='things.Thing', verbose_name='parent'),
        ),
        migrations.AddConstraint(
            model_name='thing',
            constraint=models.UniqueConstraint(fields=('description',), name='unique_thing_description'),
        ),
        migrations.AddConstraint(
            model_name='identifiable',
            constraint=models.UniqueConstraint(fields=('thing', 'serial_number'), name='unique_thing_identifiable'),
        ),
    ]
