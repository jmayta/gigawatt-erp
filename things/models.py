from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils.translation import gettext_lazy as _

from sunqu.behaviors import Timestampable
from universal.models import Country, UnitOfMeasurement


class Thing(Timestampable, models.Model):
    """Artículos"""
    KIND_STORABLE = "st"  # Almacenable
    KIND_CONSUMABLE = "cs"  # Consumible
    KIND_SERVICE = "sv"  # Servicio
    KIND_CHOICES = (
        (KIND_STORABLE, _("Storable")),
        (KIND_CONSUMABLE, _("Consumable")),
        (KIND_SERVICE, _("Service")),
    )

    # Código de artículo
    code = models.CharField(_("code"), blank=True, default="", max_length=16)
    # Código UNSPSC
    unspsc = models.CharField(blank=True, default="", max_length=8)
    # Descripción
    description = models.CharField(
        _("description"),
        max_length=255
    )
    # Unidad de medidad predeterminada
    default_uom = models.ForeignKey(
        UnitOfMeasurement,
        on_delete=models.PROTECT,
        verbose_name=_("unit of measurement")
    )
    # Tipo de artículo
    kind = models.CharField(
        _("kind"),
        max_length=2,
        choices=KIND_CHOICES,
        default=KIND_CONSUMABLE
    )
    # Imagen referencial
    image = models.ImageField(
        _("image"),
        blank=True,
        null=True,
        upload_to="things/images/",
    )
    # Peso del artículo (kg)
    weight = models.FloatField(
        _("weight"),
        default=0.00
    )
    # padre
    parent = models.ForeignKey(
        "self",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="instances",
        verbose_name=_("parent")
    )
    # Componentes
    components = models.ManyToManyField(
        "self",
        symmetrical=False,
        through="Composition"
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["description"], name="unique_thing_description"
            )
        ]
        ordering = [
            "description",
        ]
        verbose_name = _("thing")
        verbose_name_plural = _("things")

    def __str__(self):
        return f"{self.description}"


class Composition(models.Model):
    """
    Componentes

    Los componentes son artículos agrupados que conforman un artículo complejo.

    Por ejemplo:
    ------------
    - Kit de rescate
        Es conformado por:
        - Soga driza de 1/2" x 200 m (AMERICAN CORD)
        - Arnés de rescatista 6 Anillas 7 Hebillas (HAUK)
    """
    # objeto padre
    parent = models.ForeignKey(
        Thing,
        on_delete=models.CASCADE,
        related_name="children",
        verbose_name=_("parent")
    )
    # componente / hijo
    component = models.ForeignKey(
        Thing,
        on_delete=models.PROTECT,
        related_name="parents",
        verbose_name=_("component")
    )
    # cantidad
    quantity = models.FloatField(_("quantity"), blank=True, default=1.0)

    def __str__(self):
        return f"{self.parent} / {self.component}"


class Identifiable(models.Model):
    # objeto / cosa / artículo
    thing = models.ForeignKey(
        Thing,
        on_delete=models.PROTECT,
        related_name="identifications",
        verbose_name=_("thing")
    )
    # nro de serie
    serial_number = models.CharField(
        _("serial number"),
        max_length=100
    )
    # lote
    lot = models.CharField(_("lot"), blank=True, default="", max_length=50)
    # es propio (nuestro)
    is_owned = models.BooleanField(_("is owned"), default=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["thing", "serial_number"],
                name="unique_thing_identifiable"
            )
        ]
        verbose_name = _("identifiable")
        verbose_name_plural = _("identifiables")

    def __str__(self):
        return f"{self.thing} / {self.serial_number}"
