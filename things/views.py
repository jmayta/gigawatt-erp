from functools import reduce
from operator import or_, and_

from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import render, redirect
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView
)

from .models import Thing
# from purchases.models import PurchaseLine
from stocks.models import Stock


class ThingList(ListView):
    model = Thing
    queryset = Thing.objects.prefetch_related("serials").all()


class ThingDetail(DetailView):
    model = Thing

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["stock"] = Stock.objects.filter(thing=self.object)
        return context


class ThingCreate(CreateView):
    model = Thing
    fields = ["code", "unspsc", "description", "default_measure"]


# def search_thing(request):
#     query = request.GET.get("q", None)  # obtener "query"
#     # si "query" es nulo o en blanco...
#     if query is None or query == "":
#         # ...ir a la lista global de artículos
#         return redirect("things:list")
#     # obtener lista de palabras ingresadas
#     array_words = query.split(" ")
#     # permutar la función "y" para cada palabra en la lista de palabras
#     queryset = Thing.objects.filter(
#         reduce(
#             and_,
#             (Q(description__icontains=word) for word in array_words)
#         )
#     )
#     # obtener el valor del querystring "page"
#     num_page = request.GET.get("page")
#     # crear paginación con 20 registro por página
#     paginator = Paginator(queryset, 20)
#     # obtener página determinada
#     page_obj = paginator.get_page(num_page)  # retorna un objeto Page

#     return render(request, "things/thing_search.html", {
#         "query": query,
#         "object_list": page_obj.object_list,
#         "page_obj": page_obj,
#         "paginator": paginator,
#     })


# def service_list(request):
#     queryset = Thing.objects.filter(kind="SV")
#     return render(
#         request,
#         "things/service_list.html",
#         context={"object_list": queryset})
