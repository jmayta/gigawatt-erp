from django.contrib import admin

from import_export.admin import ImportExportModelAdmin

from .models import Composition, Identifiable, Thing
from maintenances.models import Maintenance


# in-lines
class IdentifiableInLine(admin.TabularInline):
    model = Identifiable


class MaintenanceInLine(admin.TabularInline):
    model = Maintenance


# admin models
class IdentifiableAdmin(admin.ModelAdmin):
    inlines = [
        MaintenanceInLine,
    ]    
    model = Identifiable


class ThingAdmin(ImportExportModelAdmin):
    exclude = (
        "created_at",
        "updated_at",
    )

    inlines = [
        IdentifiableInLine,
    ]

    list_display = [
        'id',
        'description',
    ]

    search_fields = [
        'description',
    ]


admin.site.register(Composition)
admin.site.register(Identifiable, IdentifiableAdmin)
admin.site.register(Thing, ThingAdmin)
