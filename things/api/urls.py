from django.urls import path

from things.api import views


urlpatterns = [
    path("", views.ThingListCreateAPIView.as_view(), name="thing-list"),
    path(
        "<int:pk>/",
        views.ThingRetrieveUpdateDestroyAPIView.as_view(),
        name="thing-detail"
    ),
]
