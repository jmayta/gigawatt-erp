from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ThingsConfig(AppConfig):
    name = 'things'
    verbose_name = _("things")
