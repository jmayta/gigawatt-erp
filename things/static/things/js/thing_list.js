document.addEventListener("DOMContentLoaded", function() {
    $(".datatable-responsive-column-controlled").DataTable({
        responsive: {
            details: {
                type: "column"
            }
        },
        columnDefs: [
            {
                className: "control",
                orderable: false,
                targets: 0,
                width: "30px",
                defaultContent: ""
            },
            {
                targets: 1,
                width: "100px"
            },
            {
                targets: 3,
                width: "110px"
            },
            {
                targets: 4,
                width: "150px"
            },
            {
                orderable: false,
                targets: -1,
                width: "30px"
            }
        ]
    });
});
