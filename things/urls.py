from django.urls import path

from .views import (
    ThingList,
    ThingDetail,
)

app_name = "things"

urlpatterns = [
    path("", ThingList.as_view(), name="list"),
    path("<int:pk>", ThingDetail.as_view(), name="detail"),
]
